# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###


* This is Java based Web-project named Hand Book.
* Here you can create/read/update/delete/find any contact.
* Each contact has adress, avatar, attachment.
* You can add a file to any contact.(Max size ~ 4mb).
* Technologies are JSP,Servlets(3.1.0), JDBC,MySQL , JavaScript,Commons Upload,sl4j, Bootstrap,String Templates v4, Quartz, Java Mail API.


### How do I get set up? ###

* Set up your environment for Java
* Set up your environment for Tomcat
* Create Java Web Application 
* Add Maven
* Create database named **handbook**, using Mysql WorkBench
* In folder **resources** you will find  file:_database.properties_ . 
* In this file you need to change db_login, db_pass. (Login and pass to your database).
* Then execute scripts : database.sql , testvalues.sql.

