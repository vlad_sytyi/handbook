<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <meta charset="utf-8">
    <link href="front-end/css/style.css" rel="stylesheet" type="text/css">

    <link href="front-end/css/bootstrap.css" rel="stylesheet" type="text/css">
    <title>Main page</title>
</head>
<body>

<!-- Navigation -->
<%@include file="navbar.jsp" %>

<div class="container">
    <div class="header-panel">Contacts

    </div>
    <hr>
    <form id="contact-form">
        <div class="row">
            <div class="col-md-12 ">

                <div class="table-responsive">
                    <table class="table-bordered table-hover" id="contactsTable">
                        <thead>
                        <tr>
                            <th class="col-md-1 text-center table-head-cell">
                                <input type="checkbox" id="userIds" onclick="setAllChecked()">
                            </th>
                            <th class="col-md-3 text-center table-head-cell">

                                <strong>Full Name</strong>

                            </th>
                            <th class="col-md-1 text-center table-head-cell">
                                <strong>Date of birth</strong>
                            </th>
                            <th class="col-md-3 text-center table-head-cell">
                                <strong> Address</strong>
                            </th>
                            <th class="col-md-1 text-center table-head-cell">
                                <strong>Job</strong>
                            </th>
                            <th class="col-md-1 text-center table-head-cell">

                            </th>

                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="users" items="${usersList}">

                            <tr>
                                <td class="col-md-1 text-center table-body-cell">

                                    <input type="checkbox" id="userId" name="selected_contacts" value="${users.personId}">
                                </td>
                                <td class="col-md-3 text-center table-body-cell">

                                        ${users.firstName} ${users.secondName} ${users.middleName}

                                </td>
                                <td class="col-md-1 text-center table-body-cell">

                                        ${users.birthDate}

                                </td>
                                <td class="col-md-3 text-center table-body-cell">

                                        ${users.country} ${users.city} ${users.street} ${users.houseNumber} ${users.flatNumber} ${users.index}

                                </td>
                                <td class="col-md-1 text-center table-body-cell">

                                        ${users.job}
                                </td>
                                <td class="col-md-1 text-center table-body-cell">

                                        <%--<a href="/?Command=ShowPersonDetails&userid=${users.id}" class="btn btn-primary btn-xs btn-group">Комментировать</a>--%>

                                        <%--<button type="submit" onclick="showPerson(this.value)" class="btn btn-primary btn-xs btn-group" title="Show"  value="${users.id}">--%>
                                        <%--<span class="glyphicon glyphicon-user"></span>--%>
                                        <%--</button>--%>

                                    <form action="/" id="show-btn-form" class="btn-group" method="post">

                                        <input type="hidden" name="Command" value="ShowPersonDetails">
                                        <input type="hidden" id="personId" name="userid" value="${users.personId}">
                                        <input type="submit" class="btn btn-primary btn-xs btn-group" title="Show"
                                               value="Show">
                                    </form>

                                </td>

                            </tr>

                        </c:forEach>
                        </tbody>
                    </table>

                    <div class="buttons-mainpage">


                        <input type="button" value="DELETE" class="btn btn-primary" id="deleteMainPageButton"
                               onclick="deleteCheckedContacts()">
                        <input type="button" value="SEND MAIL" class="btn btn-primary" id="sendMailMainPageButton"
                               onclick="sendEmailToCheckedContacts()">

                    </div>



                </div>
                <nav id = "paginationNav" class="col-md-7 col-md-offset-5" aria-label="Page navigation">
                    <ul class="pagination ">
                        <%--For displaying Previous link except for the 1st page --%>
                        <li>
                            <c:if test="${currentPage != 1}">
                                <a href="?Command=ShowData&page=${currentPage - 1}" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </c:if>
                        </li>

                        <%--For displaying Page numbers.
                          The when condition does not display a link for the current page--%>
                        <c:forEach begin="1" end="${noOfPages}" var="i">
                            <c:choose>
                                <c:when test="${currentPage eq i}">
                                    <li class="active"><a href="?Command=ShowData&page=${i}">${i} <span
                                            class="sr-only">(current)</span></a></li>
                                </c:when>
                                <c:otherwise>
                                    <li><a href="?Command=ShowData&page=${i}">${i}</a></li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <%--For displaying Next link --%>
                        <li>
                            <c:if test="${currentPage lt noOfPages}">
                                <a href="?Command=ShowData&page=${currentPage + 1}" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </c:if>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

    </form>


</div>


</body>
<script src="front-end/js/mainpage-js.js"></script>
</html>
