<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 ">
                <div class="navbar-header">

                    <h2>Contacts</h2>

                </div>

                <div class="navbar-form navbar-left navbar-cell-size">

                    <form action="/">
                        <input type="hidden" name="Command" value="ShowData">
                        <button type="submit" class="btn btn-info navbar-btn">
                            <span class="glyphicon glyphicon-home"></span>
                            Main
                        </button>
                    </form>

                </div>
                <div class="navbar-form navbar-left navbar-cell-size">
                    <form action="/">
                        <input type="hidden" name="Command" value="NewContact">
                        <button type="submit" class="btn btn-info navbar-btn">
                            <span class="glyphicon glyphicon-user"></span>
                            New Contact
                        </button>

                    </form>
                </div>
                <%--<div class="navbar-form navbar-left navbar-cell-size">--%>
                    <%--<form action="/">--%>
                        <%--<input type="hidden" name="Command" value="MailPage">--%>
                        <%--<button type="submit" class="btn btn-info navbar-btn">--%>
                            <%--<span class="glyphicon glyphicon-envelope"></span>--%>
                            <%--Send Message--%>
                        <%--</button>--%>
                    <%--</form>--%>
                <%--</div>--%>


                <div class="navbar-form navbar-left navbar-cell-size">
                    <form action="/">
                        <input type="hidden" name="Command" value="SearchPage">
                        <button type="submit" class="btn btn-info navbar-btn">
                            <span class="glyphicon glyphicon-search"></span>
                            Search
                        </button>
                    </form>
                </div>
                <%--<div class="navbar-form navbar-left navbar-cell-size">--%>
                    <%--<form action="/">--%>
                        <%--<input type="hidden" name="Command" value="PersonDetails">--%>
                        <%--<button type="submit" class="btn btn-info navbar-btn    ">--%>
                            <%--<span class="glyphicon glyphicon-user"></span>--%>
                            <%--Person Details--%>
                        <%--</button>--%>
                    <%--</form>--%>
                <%--</div>--%>
            </div>
        </div>
    </div>
</nav>
