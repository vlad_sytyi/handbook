<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <meta charset="utf-8">
    <link href="../front-end/css/style.css" rel="stylesheet" type="text/css">
    <link href="../front-end/css/bootstrap.css" rel="stylesheet" type="text/css">
    <title>Send Mail</title>
</head>
<body>

<!-- Navigation -->
<%@include file="navbar.jsp" %>

<div class="container-fluid">


    <div class="mail-form col-md-12">


        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-default text-left">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-6">
                            <h2>Sending email </h2><br>
                        </div>

                    </div>
                    <form id="contact-form" method="post" action="/?Command=SendMessage" role="form">
                        <div class="controls">
                            <div class="email-header">
                                <div class="row">
                                    <%--<div class="col-md-6">--%>
                                    <%--<label for="form-email-to">Email *</label>--%>
                                    <%--<div class="input-group">--%>
                                    <%--<input id="form-email-to" type="email" name="email-to" class="form-control"--%>
                                    <%--placeholder="Please enter email *" >--%>


                                    <%--</div>--%>
                                    <%--<div class="hidden" id="email-error-box">--%>
                                    <%--<div class="alert alert-danger">--%>
                                    <%--<strong>Invalid input data!</strong> check input email address...--%>
                                    <%--</div>--%>
                                    <%--</div>--%>
                                    <%--</div>--%>

                                    <div class="col-md-6">
                                        <table class="table emails">
                                            <tbody id="table-body-emails-to-send">
                                            <c:forEach var="email" items="${personEmails}">
                                                <tr>
                                                    <td class="col-md-11">
                                                        <input id="emailsToSend" readonly name="emails"
                                                               value="${email.eMail}">
                                                        <input type="hidden" id="emailsToSendId" name="emailsId"
                                                               value="${email.personId}">
                                                    </td>
                                                    <td class="col-md-1">
                                                        <button class="btn btn-sm btn-danger"
                                                                onclick="removeEmailFromSendList(this)">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </td>


                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                            <div class="messages"></div>


                            <div class="row">
                                <div class="col-md-6 select-template">
                                    <div class="form-group">
                                        <label for="form-subject">Subject *</label>
                                        <input id="form-subject" type="text" name="subject" class="form-control"
                                               placeholder="Please enter subject *" required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 select-template">
                                    <div class="form-group">
                                        <label for="sel1">Select template:</label>
                                        <select class="form-control" name = "email-template"; id="sel1" onChange="changeMessageField(this.value)">
                                            <option selected></option>
                                            <option>Birthday</option>
                                            <option>Christmas</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="hidden">

                                <input type="text" id="email-template" name="email-template" value="Default">
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form-message">Message *</label>
                                        <textarea id="form-message" name="message" class="form-control"
                                                  placeholder="Message body *" rows="4"
                                                  required="required"
                                        ></textarea>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <input type="submit" id="sendMessageButton" class="btn btn-success btn-send"
                                           onclick="return checkValues()" value="Send message">
                                </div>
                                <div class="col-md-6">
                                    <c:if test="${wasSend == true}">
                                        <div class="alert alert-success">
                                            <strong>Email was successfully sent!</strong>
                                        </div>
                                    </c:if>
                                    <c:if test="${wasSend == false}">
                                        <div class="alert alert-warning">
                                            <strong>Email wasn't sent!</strong>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <br>
                                    <p class="text-muted"><strong>*</strong> These fields are required.</p>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>


</div>

<script src="front-end/js/sendMail-js.js"></script>
</body>
</html>
