<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>

    <link href="/front-end/css/style.css" rel="stylesheet" type="text/css">
    <link href="/front-end/css/bootstrap.css" rel="stylesheet" type="text/css">
    <!-- Javascript functions -->
    <script src="/front-end/js/person-details-image-changing-js.js"></script>


    <title>PersonDetails</title>
</head>
<body>

<!-- Navigation -->
<%@include file="navbar.jsp"%>

<div class="container-fluid">


    <form id = "myForm"  method="post" enctype="multipart/form-data">

        <div class="person-info-full col-md-12">


            <div class="person-info col-md-2">
                <label for="ava">
                    <img src="/?Command=personImage&userid=${userData.personId}" onerror="checkSRC()" id="ava-img"
                         class="col-md-12"/></label>
                <input type="file" id="ava"  name="avatar" onchange="onImageChange(this.value)" size="50"/>
                <span id="fotoSpan" style="display: none; color: #5cb85c">Foto will be changed after the whole contact will be saved</span>
                <br/>
                <%--<input type="submit" id="submit-btn-avatar" value="Upload File"/>--%>

            </div>


            <div class="person-info col-md-4 ">





                <%--<input type="hidden" name="Command" value="SaveData">--%>

                <input type="submit" value="Save" class="btn btn-success" onclick="sendFormData()"  title="save">


                <div class="person-info-main col-md-12">

                    <h3>Main data</h3>

                    <div class="prsn-data col-md-12 ">
                        <label for="fname">First Name</label>
                        <br>
                        <input type="text" id="fname" class="form-control" value="${userData.firstName}"
                               name="firstname" onchange="onFirtsNameChange(this.value)"
                               placeholder="Your first name.." pattern="^[a-zA-Z\sА-Яа-я]*$" required>
                        <span id="fnameSpan" style="display: none; color: red">Name can be only letters. Max lenght is 15 symbols</span>
                        <input type="hidden" id = "userId" value="${userData.personId}" name="userId">


                    </div>

                    <div class="form-group col-md-12 ">
                        <label for="lname">Second Name</label>
                        <br>
                        <input type="text" id="lname" class="form-control" value="${userData.secondName}"
                               name="lastname" onchange="onSecondNameChange(this.value)"
                               placeholder="Your second name.." pattern="^[a-zA-Z\sА-Яа-я]*$" maxlength="15" required>
                        <span id="lnameSpan" style="display: none; color: red">Second name can be only letters. Max lenght is 15 symbols</span>
                    </div>

                    <div class="prsn-data col-md-12">
                        <label for="mname">Middle Name</label>
                        <br>
                        <input type="text" id="mname" class="form-control" value="${userData.middleName}"
                               name="middlename" onchange="onMiddleNameChange(this.value)"
                               placeholder="Your middle name.." pattern="^[a-zA-Z\sА-Яа-я]*$">
                        <span id="MnameSpan" style="display: none; color: red">Middle name can be only letters. Max lenght is 15 symbols</span>
                    </div>

                    <div class="prsn-data col-md-12">
                        <label for="bdate">Birth Date</label>
                        <br>
                        <input type="date" id="bdate" class="form-control" value="${userData.birthDate}"
                               name="birthdate">
                    </div>

                    <div class="prsn-data col-md-12">
                        <label for="sex">Sex</label>
                        <br>
                        <select id="sex" class="form-control" name="sex">
                            <option hidden selected value="${userData.sex}"> ${userData.sex} </option>
                            <option value="male">MALE</option>
                            <option value="female">FEMALE</option>
                        </select>
                    </div>

                    <div class="prsn-data col-md-12">
                        <label for="maritalStatus">Status</label>
                        <br>
                        <select id="maritalStatus" class="form-control" name="marital status">
                            <option hidden selected
                                    value="${userData.maritalStatus}"> ${userData.maritalStatus} </option>
                            <option value="single">SINGLE</option>
                            <option value="married">MARRIED</option>
                            <option value="widowed">WIDOWED</option>
                        </select>
                    </div>

                    <div class="prsn-data col-md-12">
                        <label for="pnation">Nationality</label>
                        <br>
                        <input type="text" id="pnation" value="${userData.nationality}" class="form-control"
                               name="nationality" placeholder="Nationality">
                    </div>

                    <div class="prsn-data col-md-12">
                        <label for="website">Web Site</label>
                        <br>
                        <input type="text" id="website" value="${userData.website}" class="form-control" name="website"
                               placeholder="Web Site">
                    </div>

                    <div class="prsn-data col-md-12">
                        <label for="email">E-mail</label>
                        <br>
                        <input type="text" id="email" onchange="onMailChange(this.value)" value="${userData.eMail}"
                               class="form-control" name="email"
                               placeholder="E-mail" pattern="^[a-zA-Z\.@0-9]*$">
                        <span id="emailSpan" style="display: none; color: red">Email can be only English letters with @ and .(Dot symbol).</span>
                    </div>

                    <div class="prsn-data col-md-12">
                        <label for="job">Job</label>
                        <br>
                        <input type="text" id="job" value="${userData.job}" class="form-control" name="job"
                               placeholder="Name of your company">
                    </div>

                    <br>
                </div>


            </div>

            <div class="person-info col-md-4 ">

                <div class="person-info-adress col-md-12 ">

                    <h3>Adress details</h3>

                    <div class="prsn-data col-md-12">

                    </div>
                    <div class="prsn-data col-md-12">
                        <label for="country">Country</label>
                        <br>
                        <input type="text" id="country" value="${userData.country}" class="form-control" name="country"
                               placeholder="Country" onchange="onCountryChange(this.value)"
                               pattern="^[а-яА-Я-\. ]{0,30}$|^[a-zA-Z-\. ]{0,30}$" maxlength="30">
                        <span id="countrySpan" style="display: none; color: red">Contry can be only letters.Also It can be (-). Max lenght is 30 symbols</span>
                    </div>

                    <div class="prsn-data col-md-12">
                        <label for="city">City</label>
                        <br>
                        <input type="text" id="city" value="${userData.city}" class="form-control" name="city"
                               placeholder="City" onchange="onCityChange(this.value)"
                               pattern="^[а-яА-Я0-9-\. ]{0,30}$|^[a-zA-Z0-9-\. ]{0,30}$" maxlength="30">
                        <span id="citySpan" style="display: none; color: red">City can be letters or letters with numbers.Also It can be (-). Max lenght is 30 symbols</span>
                    </div>

                    <div class="prsn-data col-md-12">
                        <label for="street">Street</label>
                        <br>
                        <input type="text" id="street" value="${userData.street}" class="form-control" name="street"
                               placeholder="Street">
                    </div>

                    <div class="prsn-data col-md-12  ">
                        <label for="houseNumber">House number</label>
                        <br>
                        <input type="text" id="houseNumber" value="${userData.houseNumber}" class="form-control"
                               name="houseNumber"
                               placeholder="House number">
                    </div>

                    <div class="prsn-data col-md-12">
                        <label for="flatNumber">Flat number</label>
                        <br>
                        <input type="text" id="flatNumber" value="${userData.flatNumber}" class="form-control"
                               name="flatNumber"
                               placeholder="Flat number">
                    </div>

                    <div class="prsn-data col-md-12 ">
                        <label for="indexOfcity">Index</label>
                        <br>
                        <input type="text" id="indexOfcity" value="${userData.index}" class="form-control"
                               name="indexOfcity" onchange="onIndexChange(this.value)" pattern="^[0-9]{0,6}$"
                               placeholder="Index Of City" maxlength="6">
                        <span id="indexSpan" style="display: none; color: red">Index can be only numbers. Max lenght is 6 symbols</span>
                    </div>
                </div>

            </div>

            <div class="person-info col-md-6 ">


                <div class="person-info-tel col-md-12 ">
                    <h3>Telephone</h3>

                    <table class="table table-bordered " id="telephone-tbl">
                        <thead>
                        <tr>
                            <th class="col-md-2 text-center">
                                <input type="checkbox" id="telephones_checked" onclick="setAllTelephonesChecked()">
                            </th>
                            <th class="col-md-4 text-center">
                                Number
                            </th>
                            <th class="col-md-3 text-center">
                                Type
                            </th>
                            <th class="col-md-3 text-center">
                                Comment
                            </th>
                        </tr>
                        </thead>
                        <tbody id = "tbodyTelephones" >
                        <c:forEach var="telephone" items="${telephones}">
                            <tr>
                                <td class="col-md-2 text-center">

                                    <input type="checkbox" id="checkBoxTelephone" name="checkedTelephones">

                                </td>
                                <td class="col-md-4 text-center ">
                                    <input class="tel-tabl-input-attr" readonly name="inputTelephone"
                                           value="${telephone.countryCode}-${telephone.providerCode}-${telephone.telephoneNumber}">
                                    <input type="hidden" value="${telephone.telephoneId}" name="telId">

                                </td>
                                <td class="col-md-3 text-center">

                                    <input class="tel-tabl-input-attr" readonly name="inputTelephoneType"
                                           value="${telephone.telephoneType}">

                                </td>
                                <td class="col-md-3 text-center">

                                    <input class="tel-tabl-input-attr" readonly name="inputTelephoneComment"
                                           value="${telephone.comment}">

                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>

                    <div id="telephone-table-buttons">

                        <input type="hidden" id="commandModal" value="">
                        <input type="button" value="ADD" class="tbl-tel-buttons btn btn-info col-md-4" id="addTelBtn">
                        <input type="button" value="UPDATE" class="tbl-tel-buttons btn btn-info col-md-4"
                               id="updtTelBtn">
                        <input type="button" value="DELETE" onclick="deleteMarkedTelephones()"
                               class="tbl-tel-buttons btn btn-info col-md-4"
                               id="delTelBtn">
                    </div>

                    <div id="telephone-modal" class="modal">
                        <div class="modal-content" id="teleph-modal">
                            <span class="close" onclick="closeModalTelephone()">&times;</span>
                            <input type="text" class="modal-data" id="country-code" placeholder="Coountry code"
                                   pattern="^[0-9]{0,6}$" onchange="onCountryCodeModalChange(this.value)">
                            <span id="countryCodeModalSpan" style="display: none; font-size: 10px; color: red">Country code can be only numbers. Max lenght is 6 symbols</span>
                            <input type="text" class="modal-data" id="operator-code" placeholder="Operator code"
                                   pattern="^[0-9]{0,6}$" onchange="onOperatorCodeModalChange(this.value)">
                            <span id="operatorCodeModalSpan" style="display: none; font-size: 10px; color: red">Operator code can be only numbers. Max lenght is 6 symbols</span>
                            <input type="text" class="modal-data" id="tel-number" placeholder="Telephone number"
                                   pattern="^[0-9]{0,6}$" onchange="onTelNumberModalChange(this.value)">
                            <span id="telNumberModalSpan" style="display: none; font-size: 10px; color: red">Telephone number can be only numbers. Max lenght is 7 symbols</span>
                            <input type="text" id="tel-type" class="modal-data" placeholder="Telephone type"
                                   pattern="^[a-zA-Z\sА-Яа-я]*$" onchange="onTelTypeModalChange(this.value)">
                            <span id="telTypeModalSpan" style="display: none; font-size: 10px ;color: red">Telephone number can be only numbers. Max lenght is 7 symbols</span>
                            <input type="text" class="modal-data" id="comment" placeholder="Comment">
                            <input type="button" value="SAVE"  class="btn btn-info col-md-4"
                                   id="saveTelBtnModal">

                        </div>
                    </div>

                </div>
            </div>


            <div class="person-info col-md-6 ">

                <div class="person-info-attachments col-md-12">
                    <h3>Attachments</h3>
                    <table class="table table-bordered " id="attachment-table">
                        <thead>
                        <tr>
                            <th class="col-md-1 text-center">
                                <input type="checkbox" id="attachment_checkbox" onclick="setAllAttachmentsChecked()">
                            </th>
                            <th class="col-md-3 text-center">
                                File name
                            </th>
                            <th class="col-md-3 text-center">
                                Date of upload
                            </th>
                            <th class="col-md-3 text-center">
                                Comment
                            </th>
                            <th class="col-md-2 text-center">

                            </th>

                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="attachment" items="${attachments}">
                        <tr>
                            <td class="col-md-1 text-center">
                                <input type="checkbox" id="checkBoxAttachment" name="checkedAttachmesnts">
                                <input type="hidden" value="${attachment.id}" name="attach-id">
                            </td>
                            <td class="col-md-3 text-center">

                                <input class="attach-tabl-input-attr" readonly name="attach-file-name"
                                       value="${attachment.attachmentName}">
                                <input type="hidden" value="${attachment.id}" name="attach-id">

                            </td>
                            <td class="col-md-3 text-center">

                                <input  class="attach-tabl-input-attr" readonly name="attach-date-of-load"
                                       value="${attachment.dateOfLoad}">


                            </td>
                            <td class="col-md-3 text-center">

                                <input class="attach-tabl-input-attr" readonly name="attach-comment"
                                       value="${attachment.comment}">


                            </td>
                            <td class="col-md-2 text-center">

                                <a href="/?Command=personAttachment&attachmentid=${attachment.id}"><span class="glyphicon glyphicon-download-alt"></span></a>
                            </td>
                        </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <div id="attachment-table-buttons">

                        <input type="button" value="ADD" class="tbl-tel-buttons btn btn-info col-md-4" id="addAttchBtn">

                        <input type="button" value="UPDATE" class="tbl-tel-buttons btn btn-info col-md-4"
                               id="updtAttchBtn">
                        <input type="button" value="DELETE" class="tbl-tel-buttons btn btn-info col-md-4"
                               id="delAttchBtn" onclick="deleteAttachment()">
                        <span id="addBtnModalSpan" style="display: none; font-size: 12px ;color: #4cae4c">File will be available after the whole contact is saved</span>
                    </div>

                    <div id="attachment-modal" class="modal">
                        <div class="modal-content " id="attachm-modal">
                            <span class="close" id="attachment-modal-close-button" onclick="closeModalAttachment()">&times;</span>
                            <input type="text" class="modal-data col-md-12" id="attachment-comment-modal"
                                   placeholder="Comment" >
                            <input type="file" name="attachment-file" class="modal-data" id="attachment-table-input">
                            <input type="text" class="modal-data col-md-12" id="attachment-filename-modal"
                            placeholder="FileName" style="display: none">
                            <input type="button" value="SAVE"   class="btn btn-info col-md-12"
                                   id="saveAttchBtnModal">
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </form>
</div>

<script src="/front-end/js/person-details-dom-js.js"></script>
<script src="/front-end/js/person-details-validator.js"></script>
</body>


</html>
