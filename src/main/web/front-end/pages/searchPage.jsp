<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <meta charset="utf-8">
    <link href="/front-end/css/style.css" rel="stylesheet" type="text/css">

    <link href="/front-end/css/bootstrap.css" rel="stylesheet" type="text/css">

    <title>Search</title>
</head>
<body>
<!-- Navigation -->
<%@include file="navbar.jsp"%>


<div class="container-fluid">

    <form action="/" method="post" >


        <div class="Search-fields col-md-6 col-md-offset-3">

            <div class="prsn-data col-md-4 ">
                <label for="fname">First Name</label>
                <br>
                <input type="text" id="fname" class="form-control" name="firstname"
                       placeholder="Your first name..">
            </div>

            <div class="prsn-data col-md-4">
                <label for="lname">Second Name</label>
                <br>
                <input type="text" id="lname" class="form-control" name="lastname"
                       placeholder="Your second name..">
            </div>

            <div class="prsn-data col-md-4">
                <label for="mname">Middle Name</label>
                <br>
                <input type="text" id="mname" class="form-control" name="middlename"
                       placeholder="Your middle name..">
            </div>

            <div class="prsn-data col-md-4">
                <label for="bdatef">Birth Date From</label>
                <br>
                <input type="date" id="bdatef" class="form-control" name="birthdatefrom">
            </div>

            <div class="prsn-data col-md-4">
                <label for="bdatet">Birth Date To</label>
                <br>
                <input type="date" id="bdatet" class="form-control" name="birthdateto">
            </div>

            <div class="prsn-data col-md-4">
                <label for="sex">Sex</label>
                <br>
                <select id="sex" class="form-control" name="sex">
                    <option selected value=""></option>
                    <option value="male">MALE</option>
                    <option value="female">FEMALE</option>
                </select>
            </div>

            <div class="prsn-data col-md-4">
                <label for="maritalStatus">Status</label>
                <br>
                <select id="maritalStatus" class="form-control" name="marital status">
                    <option selected value=""></option>
                    <option value="single">SINGLE</option>
                    <option value="married">MARRIED</option>
                    <option value="widdowed">WIDDOWED</option>
                </select>
            </div>

            <div class="prsn-data col-md-4">
                <label for="country">Country</label>
                <br>
                <input type="text" id="country" class="form-control" name="country"
                       placeholder="Country">
            </div>

            <div class="prsn-data col-md-4">
                <label for="city">City</label>
                <br>
                <input type="text" id="city" class="form-control" name="city"
                       placeholder="City">
            </div>

            <div class="prsn-data col-md-4">
                <label for="street">Street</label>
                <br>
                <input type="text" id="street" class="form-control" name="street"
                       placeholder="Street">
            </div>

            <div class="prsn-data col-md-4  ">
                <label for="houseNumber">House number</label>
                <br>
                <input type="text" id="houseNumber" class="form-control"
                       name="houseNumber"
                       placeholder="House number">
            </div>

            <div class="prsn-data col-md-4">
                <label for="flatNumber">Flat number</label>
                <br>
                <input type="text" id="flatNumber" class="form-control"
                       name="flatNumber"
                       placeholder="Flat number">
            </div>

            <div class="prsn-data col-md-4 ">
                <label for="indexOfcity">Index</label>
                <br>
                <input type="text" id="indexOfcity" class="form-control"
                       name="indexOfcity"
                       placeholder="Index Of City">
            </div>


        </div>
        <div class="Search-fields col-md-6 col-md-offset-3">


                <input type="hidden" value="Search" name="Command">
                <input type="submit" value="Search" title="Search">


        </div>

        <br>


    </form>


</div>


</body>
</html>
