<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <link href="/front-end/css/style.css" rel="stylesheet" type="text/css">

    <link href="/front-end/css/bootstrap.css" rel="stylesheet" type="text/css">
    <title>Error page</title>
</head>
<body>

<div class="container-fluid">
    <div class="info col-md-10 col-lg-offset-1">

        <h1>Something wrong</h1>

        <c:forEach var="errors" items="${errors}">

            <p class="text-primary"><span class="glyphicon glyphicon-exclamation-sign"></span> ${errors}</p>
        </c:forEach>

    </div>

</div>

</body>
</html>
