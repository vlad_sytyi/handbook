const TELEPHONE_MODAL = document.getElementById("telephone-modal");
const CLOSE_MODAL_WINDOW_BUTTON = document.getElementsByClassName("close")[0];
const TELEPHONE_TABLE = document.getElementById("telephone-tbl");
const TELEPHONE_TABLE_PERSON_ID = document.getElementById("telId");
const TELEPHONE_TABLE_TELEPHONE_NUMBER = document.getElementById("inputTelephone");
const TELEPHONE_TABLE_TELEPHONE_TYPE = document.getElementById("inputTelephoneType");
const TELEPHONE_TABLE_TELEPHONE_COMMENT = document.getElementById("inputTelephoneComment");
const TELEPHONE_TABLE_FIELD_COMMAND_MODAL = document.getElementById("commandModal");
const TELEPHONE_TABLE_ADD_TEL_BUTTON = document.getElementById("addTelBtn");
const TELEPHONE_TABLE_DELETE_TEL_BUTTON = document.getElementById("delTelBtn");
const TELEPHONE_TABLE_UPDATE_TEL_BUTTON = document.getElementById("updtTelBtn");
const TELEPHONE_MODAL_COUNTRY_CODE = document.getElementById("country-code");
const TELEPHONE_MODAL_OPERATOR_CODE = document.getElementById("operator-code");
const TELEPHONE_MODAL_TEL_NUMBER = document.getElementById("tel-number");
const TELEPHONE_MODAL_TEL_TYPE = document.getElementById("tel-type");
const TELEPHONE_MODAL_SAVE_BUTTON = document.getElementById("saveTelBtnModal");
const TELEPHONE_MODAL_COMMENT = document.getElementById("comment");


const ATTACHMENTS_TABLE = document.getElementById("attachment-table");

//const ATTACHMENTS_TABLE_DOWNLOAD_BUTTON = ;
const ATTACHMENTS_TABLE_ADD_ATTACHMENT_BUTTON = document.getElementById("addAttchBtn");
const ATTACHMENTS_TABLE_UPDATE_ATTACHMENT_BUTTON = document.getElementById("updtAttchBtn");
const ATTACHMENTS_TABLE_DELETE_ATTACHMENT_BUTTON = document.getElementById("delAttchBtn");
const ATTACHMENTS_TABLE_ADD_ATTACHMENT_BUTTON_SPAN = document.getElementById("addBtnModalSpan");
const ATTACHMENTS_MODAL = document.getElementById("attachment-modal");
const ATTACHMENTS_MODAL_NAME = document.getElementById("attachment-filename-modal");
const ATTACHMENTS_MODAL_COMMENT = document.getElementById("attachment-comment-modal");
const ATTACHMENTS_MODAL_INPUT_FILE = document.getElementById("attachment-table-input");
const ATTACHMENTS_MODAL_SAVE_BUTTON = document.getElementById("saveAttchBtnModal");
const ATTACHMENTS_MODAL_CLOSE_BUTTON = document.getElementById("attachment-modal-close-button");

const TEL_NUMBER_INDEX = 1;
const TEL_TYPE_INDEX = 2;
const TEL_COMMENT_INDEX = 3;


//storage for new Attachments
var formData = new FormData;


// ///////////////////////////////
// PHONE MODAL FUNCTIONS        //
//////////////////////////////////
TELEPHONE_TABLE_ADD_TEL_BUTTON.onclick = function () {
    TELEPHONE_TABLE_FIELD_COMMAND_MODAL.value = "1";
    TELEPHONE_MODAL.style.display = "block"
};
TELEPHONE_TABLE_UPDATE_TEL_BUTTON.onclick = function () {
    TELEPHONE_TABLE_FIELD_COMMAND_MODAL.value = "0";

    // iterate over table with getCheckedValuesFromCheckBox method
    // then getting data from Telephone table and put this data to Telephone modal
    // opening modal

    var result = checkCheckboxCheckedValues();
    if (result === true) {
        getValuesFromTelephone(getCheckedValuesFromCheckBox());
        openModelTelephone();
    }
};

TELEPHONE_MODAL_SAVE_BUTTON.onclick = function () {

    var telValue = TELEPHONE_TABLE_FIELD_COMMAND_MODAL.value;

    if (telValue === "0") {
        // iterate over table with getCheckedValuesFromCheckBox method
        // then save values to checked Row in Telephone table
        // after that clear all field
        // and closes modal
        saveValuesToTelephones(getCheckedValuesFromCheckBox());
        cleanFieldsModalTelephones();
        removeCheckedCheckboxesTelephones();
        closeModalTelephone()

    } else {
        addTelephone()
    }
};

CLOSE_MODAL_WINDOW_BUTTON.onclick = function () {

    cleanFieldsModalTelephones();
    closeModalTelephone();
    closeModalAttachment();
};


var openModelTelephone = function () {
    TELEPHONE_MODAL.style.display = "block";
};

var closeModalTelephone = function () {
    TELEPHONE_MODAL.style.display = "none";
    document.getElementById("countryCodeModalSpan").style.display = "none";
    document.getElementById("operatorCodeModalSpan").style.display = "none";
    document.getElementById("telNumberModalSpan").style.display = "none";
    document.getElementById("telTypeModalSpan").style.display = "none";
    removeCheckedCheckboxesTelephones();
};

var addTelephone = function addTelephone() {

    var countryCode = TELEPHONE_MODAL_COUNTRY_CODE.value;
    var operatorCode = TELEPHONE_MODAL_OPERATOR_CODE.value;
    var telephoneNumber = TELEPHONE_MODAL_TEL_NUMBER.value;
    var telephoneType = TELEPHONE_MODAL_TEL_TYPE.value;
    var telephoneComment = TELEPHONE_MODAL_COMMENT.value;

    var tbody = TELEPHONE_TABLE.getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");
    tbody.appendChild(row);


    //creating <td> with input type = "checkbox" inside
    var tableTelCheckbox = document.createElement("TD");
    tableTelCheckbox.className = "col-md-2 text-center";
    row.appendChild(tableTelCheckbox);


    var checkBox = document.createElement("INPUT");
    checkBox.type = "checkbox";
    checkBox.id = "checkBoxTelephone";

    tableTelCheckbox.appendChild(checkBox);


    //creating <td> with input and value is Telephone Number
    var telephoneTD = document.createElement("TD");
    telephoneTD.className = "col-md-4 text-center";
    row.appendChild(telephoneTD);

    var telephone = document.createElement("INPUT");
    telephone.readOnly = true;
    telephone.className = "tel-tabl-input-attr";
    telephone.name = "inputTelephone";
    telephone.value = countryCode + "-" + operatorCode + "-" + telephoneNumber;
    telephoneTD.appendChild(telephone);


    var telephoneId = document.createElement("INPUT");
    telephoneId.type = "hidden";
    telephoneId.name = "telId";
    telephoneId.value = "6699";
    telephoneTD.appendChild(telephoneId);


    //creating <td> with input and value is Telephone Type
    var telTypeTD = document.createElement("TD");
    telTypeTD.className = "col-md-3 text-center";
    row.appendChild(telTypeTD);


    //I'll add a default value(deftype), to the field telType.value if user doesn't wan't to add a type of the phone.
    var telType = document.createElement("INPUT");
    telType.readOnly = true;
    telType.className = "tel-tabl-input-attr";
    telType.name = "inputTelephoneType";
    var telTypeTrimmed = TELEPHONE_MODAL_TEL_TYPE.value.trim();
    if (telTypeTrimmed == "") {
        telType.value = ""
    } else {
        telType.value = telephoneType;
    }
    telTypeTD.appendChild(telType);


    //creating <td> with input and value is Telephone Comment
    var telCommentTD = document.createElement("TD");
    telCommentTD.className = "col-md-3 text-center";
    row.appendChild(telCommentTD);

    //I'll add a default value(defcomm), to the field telcooment.value if you don't wan't to add a comment

    var telComment = document.createElement("INPUT");
    telComment.readOnly = true;
    telComment.className = "tel-tabl-input-attr";
    telComment.name = "inputTelephoneComment";
    var telCommentTrimmed = TELEPHONE_MODAL_COMMENT.value.trim();
    if (telCommentTrimmed == "") {
        telComment.value = ""
    } else {
        telComment.value = telephoneComment;
    }

    telCommentTD.appendChild(telComment);

    cleanFieldsModalTelephones()
};

var cleanFieldsModalTelephones = function () {

    TELEPHONE_MODAL_COUNTRY_CODE.value = "";
    TELEPHONE_MODAL_OPERATOR_CODE.value = "";
    TELEPHONE_MODAL_TEL_NUMBER.value = "";
    TELEPHONE_MODAL_TEL_TYPE.value = "";
    TELEPHONE_MODAL_COMMENT.value = "";
    TELEPHONE_MODAL.style.display = "none"

};

var deleteMarkedTelephones = function () {

    var table = document.getElementById("telephone-tbl").tBodies[0];
    var rowCount = table.rows.length;

    // var i=1 to start after header
    for (var i = 0; i < rowCount; i++) {
        var row = table.rows[i];
        // index of td contain checkbox is 0
        var chkbox = row.cells[0].getElementsByTagName('input')[0];
        if ('checkbox' == chkbox.type && true == chkbox.checked) {
            table.deleteRow(i);
            i--;
        }
    }

};

var checkCheckboxCheckedValues = function () {
    var table = document.getElementById("telephone-tbl").tBodies[0];
    var rowCount = table.rows.length;
    var count = 0;
    var result = true;
    for (var i = 0; i < rowCount; i++) {
        var row = table.rows[i];
        var rowIndex = row.rowIndex;
        var chkbox = row.cells[0].getElementsByTagName('input')[0];
        if ('checkbox' == chkbox.type && true == chkbox.checked) {

            count++;
            if (count > 1) {
                alert("You can update only one telephone. Please mark only one telephone")
                result = false;
            }
        }

    }
    if (count === 0) {
        alert("Please choose one variant for updating")
        result = false;
    }
    return result;
}


var getCheckedValuesFromCheckBox = function () {


    var table = document.getElementById("telephone-tbl").tBodies[0];
    var rowCount = table.rows.length;

    for (var i = 0; i < rowCount; i++) {
        var row = table.rows[i];
        var rowIndex = row.rowIndex;
        var chkbox = row.cells[0].getElementsByTagName('input')[0];
        if ('checkbox' == chkbox.type && true == chkbox.checked) {
            return rowIndex;
        }
    }
};

var getValuesFromTelephone = function (rowIndex) {

    var table = document.getElementById("telephone-tbl").tBodies[0];
    var row = table.rows[0].rowIndex;
    var fullNumberFromTable = document.getElementById("telephone-tbl").rows[rowIndex].cells[1].firstElementChild.value;
    var arrayTel = fullNumberFromTable.split("-");
    var countryCode = arrayTel[0];
    var providerCode = arrayTel[1];
    var telephoneNumber = arrayTel[2];
    TELEPHONE_MODAL_COUNTRY_CODE.value = countryCode;
    TELEPHONE_MODAL_OPERATOR_CODE.value = providerCode;
    TELEPHONE_MODAL_TEL_NUMBER.value = telephoneNumber;
    TELEPHONE_MODAL_TEL_TYPE.value = document.getElementById("telephone-tbl").rows[rowIndex].cells[2].firstElementChild.value;
    TELEPHONE_MODAL_COMMENT.value = document.getElementById("telephone-tbl").rows[rowIndex].cells[3].firstElementChild.value;
};

var saveValuesToTelephones = function (rowIndex) {

    document.getElementById("telephone-tbl").rows[rowIndex].cells[1].firstElementChild.value = TELEPHONE_MODAL_COUNTRY_CODE.value + "-" + TELEPHONE_MODAL_OPERATOR_CODE.value + "-" + TELEPHONE_MODAL_TEL_NUMBER.value;
    document.getElementById("telephone-tbl").rows[rowIndex].cells[2].firstElementChild.value = TELEPHONE_MODAL_TEL_TYPE.value;
    document.getElementById("telephone-tbl").rows[rowIndex].cells[3].firstElementChild.value = TELEPHONE_MODAL_COMMENT.value;

};


var removeCheckedCheckboxesTelephones = function () {
    var table = document.getElementById("telephone-tbl").tBodies[0];
    var rowCount = table.rows.length;

    for (var i = 0; i < rowCount; i++) {
        var row = table.rows[i];
        var rowIndex = row.rowIndex;
        var chkbox = row.cells[0].getElementsByTagName('input')[0];
        if ('checkbox' == chkbox.type && true == chkbox.checked) {
            chkbox.checked = false;

        }
    }
}


// /////////////////////////////////////
// ATTACHMENTS MODAL FUNCTIONS        //
////////////////////////////////////////

ATTACHMENTS_TABLE_ADD_ATTACHMENT_BUTTON.onclick = function () {
    TELEPHONE_TABLE_FIELD_COMMAND_MODAL.value = "1";

    ATTACHMENTS_MODAL.style.display = "block"
    ATTACHMENTS_MODAL_INPUT_FILE.style.display = "inline"
};

var openModalAttchment = function () {
    ATTACHMENTS_MODAL.style.display = "block"
};

var closeModalAttachment = function () {
    ATTACHMENTS_MODAL.style.display = "none";
    ATTACHMENTS_MODAL_COMMENT.value = "";
    ATTACHMENTS_MODAL_NAME.style.display = "none";
    ATTACHMENTS_MODAL_INPUT_FILE.value = "";
    removeCheckedCheckboxes();


};

ATTACHMENTS_TABLE_UPDATE_ATTACHMENT_BUTTON.onclick = function () {
    TELEPHONE_TABLE_FIELD_COMMAND_MODAL.value = "0";

    // iterate over table with getCheckedValuesFromCheckBox method
    // then getting data from Attachment table and put this data to Attachment modal
    // opening modal
    // hide field of FILE uploading. Becuase wecan change only comment.
    var result = checkCheckboxCheckedValuesAttachmentTable();
    if (result === true) {
        getValuesFromAttachment(getCheckedValuesFromCheckBoxAttachmentTable());
        ATTACHMENTS_MODAL_NAME.style.display = "inline";
        ATTACHMENTS_MODAL_INPUT_FILE.style.display = "none";
        openModalAttchment();
    }
};

ATTACHMENTS_MODAL_SAVE_BUTTON.onclick = function () {
    ATTACHMENTS_MODAL_INPUT_FILE.style.display = "block"

    var attchValue = TELEPHONE_TABLE_FIELD_COMMAND_MODAL.value;

    if (attchValue === "0") {
        // iterate over table with getCheckedValuesFromCheckBox method
        // then save values to checked Row in Attchment table
        // after that clear all field
        // and closes modal
        saveValuesToAttachments(getCheckedValuesFromCheckBoxAttachmentTable());
        cleanFielldModalAttachemtns();
        removeCheckedCheckboxes();
        closeModalAttachment();


    } else {
        if (ATTACHMENTS_MODAL_INPUT_FILE.value == "") {
            alert("Please choose the file for uploading or use X for exit")
        } else {
            // ATTACHMENTS_TABLE_ADD_ATTACHMENT_BUTTON.style.display ='none';
            //ATTACHMENTS_TABLE_ADD_ATTACHMENT_BUTTON_SPAN.style.display = "inline";
            addAttachment();
            removeCheckedCheckboxes();
            closeModalAttachment();
        }

    }

};

var addAttachment = function () {


    var comment = ATTACHMENTS_MODAL_COMMENT.value;
    var d = new Date();
    var dateOfload = d.toDateString();
    var fileName = document.getElementById("attachment-table-input").value;
    var inputFile = ATTACHMENTS_MODAL_INPUT_FILE.value;
    formData.append("attachment-table-input", ATTACHMENTS_MODAL_INPUT_FILE.files[0]);

    var tbody = ATTACHMENTS_TABLE.getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");
    tbody.appendChild(row);

    //creating <td> with input type = "checkbox" inside
    var attachmentsCheckbox = document.createElement("TD");
    attachmentsCheckbox.className = "col-md-1 text-center";
    row.appendChild(attachmentsCheckbox);

    var checkBox = document.createElement("INPUT");
    checkBox.type = "checkbox";
    checkBox.id = "checkBoxAttachment";
    attachmentsCheckbox.appendChild(checkBox);

    //creating <td> with input and value is File Name
    var attachmTD = document.createElement("TD");
    attachmTD.className = "col-md-3 text-center";
    row.appendChild(attachmTD);

    var filename = document.createElement("INPUT");
    filename.readOnly = true;
    filename.className = "attach-tabl-input-attr";
    filename.name = "attach-file-name";
    filename.value = fileName;
    attachmTD.appendChild(filename);


    var attId = document.createElement("INPUT");
    attId.type = "hidden";
    attId.name = "attach-id";
    attId.value = "6699";
    attachmTD.appendChild(attId);


    var dateOfLoadTD = document.createElement("TD");
    dateOfLoadTD.className = "col-md-3 text-center";
    row.appendChild(dateOfLoadTD);

    var dateOfLoading = document.createElement("INPUT");
    dateOfLoading.readOnly = true;
    dateOfLoading.className = "attach-tabl-input-attr";
    dateOfLoading.name = "attach-date-of-load";
    dateOfLoading.value = dateOfload;
    dateOfLoadTD.appendChild(dateOfLoading);

    var commentTD = document.createElement("TD");
    commentTD.className = "col-md-3 text-center";
    row.appendChild(commentTD);

    var commentAttch = document.createElement("INPUT");
    commentAttch.readOnly = true;
    commentAttch.className = "attach-tabl-input-attr";
    commentAttch.name = "attach-comment";
    commentAttch.value = comment;
    commentTD.appendChild(commentAttch);

    var downloadTD = document.createElement("TD");
    downloadTD.className = "col-md-2 text-center";
    row.appendChild(downloadTD);


};

var deleteAttachment = function () {

    var table = document.getElementById("attachment-table").tBodies[0];
    var rowCount = table.rows.length;

    // var i=1 to start after header
    for (var i = 0; i < rowCount; i++) {
        var row = table.rows[i];
        // index of td contain checkbox is 0
        var chkbox = row.cells[0].getElementsByTagName('input')[0];
        if ('checkbox' == chkbox.type && true == chkbox.checked) {
            table.deleteRow(i);
            i--;
        }
    }

};

var checkCheckboxCheckedValuesAttachmentTable = function () {
    var table = document.getElementById("attachment-table").tBodies[0];
    var rowCount = table.rows.length;
    var count = 0;
    var result = true;
    for (var i = 0; i < rowCount; i++) {
        var row = table.rows[i];
        var rowIndex = row.rowIndex;
        var chkbox = row.cells[0].getElementsByTagName('input')[0];
        if ('checkbox' == chkbox.type && true == chkbox.checked) {

            count++;
            if (count > 1) {
                alert("You can update only one attachment. Please mark only one attachment")
                result = false;
            }
        }
    }
    if (count === 0) {
        alert("Please choose one variant for updating")
        result = false;
    }
    return result;
};


var getCheckedValuesFromCheckBoxAttachmentTable = function () {


    var table = document.getElementById("attachment-table").tBodies[0];
    var rowCount = table.rows.length;

    for (var i = 0; i < rowCount; i++) {
        var row = table.rows[i];
        var rowIndex = row.rowIndex;
        var chkbox = row.cells[0].getElementsByTagName('input')[0];
        if ('checkbox' == chkbox.type && true == chkbox.checked) {
            return rowIndex;
        }
    }
};

var removeCheckedCheckboxes = function () {
    var table = document.getElementById("attachment-table").tBodies[0];
    var rowCount = table.rows.length;

    for (var i = 0; i < rowCount; i++) {
        var row = table.rows[i];
        var rowIndex = row.rowIndex;
        var chkbox = row.cells[0].getElementsByTagName('input')[0];
        if ('checkbox' == chkbox.type && true == chkbox.checked) {
            chkbox.checked = false;
        }
    }
}

var getValuesFromAttachment = function (rowIndex) {

    var table = document.getElementById("attachment-table").tBodies[0];
    var row = table.rows[0].rowIndex;
    ATTACHMENTS_MODAL_COMMENT.value = document.getElementById("attachment-table").rows[rowIndex].cells[3].firstElementChild.value
    ATTACHMENTS_MODAL_NAME.value = document.getElementById("attachment-table").rows[rowIndex].cells[1].firstElementChild.value
};


var saveValuesToAttachments = function (rowIndex) {


    document.getElementById("attachment-table").rows[rowIndex].cells[1].firstElementChild.value = ATTACHMENTS_MODAL_NAME.value;
    document.getElementById("attachment-table").rows[rowIndex].cells[3].firstElementChild.value = ATTACHMENTS_MODAL_COMMENT.value;


};
var cleanFielldModalAttachemtns = function () {

    ATTACHMENTS_MODAL_COMMENT.value = "";
    ATTACHMENTS_MODAL_NAME.value = "";
    ATTACHMENTS_MODAL_INPUT_FILE.value = "";

};


function setAllAttachmentsChecked() {

    var checkedAttachments = document.getElementById("attachment_checkbox").checked;
    var selectedCheckboxes = document.getElementsByName("checkedAttachmesnts");

    // loop over them all and set checked
    for (var i = 0; i < selectedCheckboxes.length; i++) {
        selectedCheckboxes[i].checked = checkedAttachments;
    }

}


function setAllTelephonesChecked() {

    var checkedTelephone = document.getElementById("telephones_checked").checked;
    var selectedCheckboxes = document.getElementsByName("checkedTelephones");

    // loop over them all and set checked
    for (var i = 0; i < selectedCheckboxes.length; i++) {
        selectedCheckboxes[i].checked = checkedTelephone;
    }


}

function sendFormData() {


    formData.append("userId", document.getElementById("userId").value)
    formData.append("firstname", document.getElementById("fname").value);
    formData.append("lastname", document.getElementById("lname").value);
    formData.append("middlename", document.getElementById("mname").value);
    formData.append("birthdate", document.getElementById("bdate").value);
    formData.append("sex", document.getElementById("sex").value);
    formData.append("marital status", document.getElementById("maritalStatus").value);
    formData.append("nationality", document.getElementById("pnation").value);
    formData.append("website", document.getElementById("website").value);
    formData.append("email", document.getElementById("email").value);
    formData.append("job", document.getElementById("job").value);
    formData.append("country", document.getElementById("country").value);
    formData.append("city", document.getElementById("city").value);
    formData.append("street", document.getElementById("street").value);
    formData.append("houseNumber", document.getElementById("houseNumber").value);
    formData.append("flatNumber", document.getElementById("flatNumber").value);
    formData.append("indexOfcity", document.getElementById("indexOfcity").value);
    if (document.getElementById("ava").value !== "") {
        formData.append("avatar", document.getElementById("ava").files[0]);
    }
    formData.append("telephones",createTelephonesJson());
    formData.append("attachments",createAttachmentsJson());
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() { // listen for state changes
        if (xhr.readyState == 4 && xhr.status == 200) { // when completed we can move away
            window.location = "/?Command=ShowData";
        }
    };
    xhr.open("POST", "/?Command=Upload",true);
    xhr.send(formData);

}

function sendFormDataCreate() {


    formData.append("firstname", document.getElementById("fname").value);
    formData.append("lastname", document.getElementById("lname").value);
    formData.append("middlename", document.getElementById("mname").value);
    formData.append("birthdate", document.getElementById("bdate").value);
    formData.append("sex", document.getElementById("sex").value);
    formData.append("marital status", document.getElementById("maritalStatus").value);
    formData.append("nationality", document.getElementById("pnation").value);
    formData.append("website", document.getElementById("website").value);
    formData.append("email", document.getElementById("email").value);
    formData.append("job", document.getElementById("job").value);
    formData.append("country", document.getElementById("country").value);
    formData.append("city", document.getElementById("city").value);
    formData.append("street", document.getElementById("street").value);
    formData.append("houseNumber", document.getElementById("houseNumber").value);
    formData.append("flatNumber", document.getElementById("flatNumber").value);
    formData.append("indexOfcity", document.getElementById("indexOfcity").value);

    if (document.getElementById("ava").value !== "") {
        formData.append("avatar", document.getElementById("ava").files[0]);
    }

    formData.append("telephones",createTelephonesJson());
    formData.append("attachments",createAttachmentsJson());
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() { // listen for state changes
        if (xhr.readyState == 4 && xhr.status == 200) { // when completed we can move away
            window.location = "/?Command=ShowData";
        }
    };
    xhr.open("POST", "/?Command=CreatePerson",true);
    xhr.send(formData);

}



function createTelephonesJson() {
    var telephones = [];
    var table = document.getElementById("telephone-tbl").tBodies[0];
    var rowCount = table.rows.length;

    for (var  i = 1; i<=rowCount; i++){
        telephones[i-1] = parseTelephone(i)
    }

    return telephonesJson = JSON.stringify(telephones);

}

function parseTelephone(rowIndex) {

    var telephone = {};
    telephone.telephoneId = parseTelephoneId(rowIndex);
    telephone.fullTelNumber = parseTelephoneNumber(rowIndex);
    telephone.telephoneType = parseTelephoneType(rowIndex);
    telephone.comment = parseTelephoneComment(rowIndex);
    return telephone;
}

function parseTelephoneId(rowIndex) {

   return document.getElementById("telephone-tbl").rows[rowIndex].cells[TEL_NUMBER_INDEX].lastElementChild.value;
}


function parseTelephoneNumber(rowIndex) {

    return document.getElementById("telephone-tbl").rows[rowIndex].cells[TEL_NUMBER_INDEX].firstElementChild.value;
}


function parseTelephoneType(rowIndex) {

    return document.getElementById("telephone-tbl").rows[rowIndex].cells[TEL_TYPE_INDEX].firstElementChild.value;
}



function parseTelephoneComment(rowIndex) {

    return document.getElementById("telephone-tbl").rows[rowIndex].cells[TEL_COMMENT_INDEX].firstElementChild.value
}




function createAttachmentsJson() {
    var attachments = [];
    var table = document.getElementById("attachment-table").tBodies[0];
    var rowCount = table.rows.length;

    for (var  i = 1; i<=rowCount; i++){
        attachments[i-1] = parseAttachments(i)
    }

    return attachmentsJson = JSON.stringify(attachments);

}


function parseAttachments(rowIndex) {

    var attachment = {};
    attachment.comment = parseAttachmentsComment(rowIndex);
    attachment.attachmentName = parseAttachmentsName(rowIndex);
    attachment.id = parseAttachmentsId(rowIndex);
    return attachment


}
function parseAttachmentsName(rowIndex) {

    return document.getElementById("attachment-table").rows[rowIndex].cells[1].firstElementChild.value;
}
function parseAttachmentsComment(rowIndex) {

    return document.getElementById("attachment-table").rows[rowIndex].cells[3].firstElementChild.value;
}
function parseAttachmentsId(rowIndex) {

    return document.getElementById("attachment-table").rows[rowIndex].cells[1].lastElementChild.value;
}



