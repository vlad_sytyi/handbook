/**
 * Created by Admin on 29.10.2017.
 */



const SEND_MESSAGE_BUTTON = document.getElementById("sendMessageButton");
const TEMPALTE_OPTION = document.getElementById("sel1");
const MESSAGE_FIELD = document.getElementById("form-message");
const EMAILS_FROM_TABLE = document.getElementsByName("emails");

function removeEmailFromSendList(element){
    var tr = element.parentElement.parentElement;
    var tBody = tr.parentElement;
    tBody.removeChild(tr);
}

function checkValues() {

    if (EMAILS_FROM_TABLE.value ===""){
        alert("You don't have any email. Please choose  some in Main page and return back");
        return false
    }else {
        return true;
    }
}
function changeMessageField() {

    if (TEMPALTE_OPTION.value ===""){
        MESSAGE_FIELD.value = templates.None;
    }
    if(TEMPALTE_OPTION.value ==="Birthday"){
        MESSAGE_FIELD.value = templates.Birthday;
    }
    if(TEMPALTE_OPTION.value ==="Christmas"){
        MESSAGE_FIELD.value = templates.Christmas;
    }

}

var templates = {
    "None" : "",
    "Birthday" : "Dear <name> Happy Birthday",
    "Christmas" : "Dear <name>, Marry Christmas and Happy New Year;"
}