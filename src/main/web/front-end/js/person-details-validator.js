/**
 * Created by Admin on 20.10.2017.
 */




function onFirtsNameChange(element) {

    regexp = /^[a-zA-Z\sА-Яа-я]*$/;
    if (regexp.test(element)) {
        document.getElementById("fnameSpan").style.display = "none"
    } else {
        document.getElementById("fnameSpan").style.display = "inline"
    }
}


function onSecondNameChange(element) {
    regexp = /^[a-zA-Z\sА-Яа-я]*$/;
    if (regexp.test(element)) {
        document.getElementById("lnameSpan").style.display = "none"
    } else {
        document.getElementById("lnameSpan").style.display = "inline"
    }
}


function onMiddleNameChange(element) {
    regexp = /^[a-zA-Z\sА-Яа-я]*$/;
    if (regexp.test(element)) {
        document.getElementById("lnameSpan").style.display = "none"
    } else {
        document.getElementById("lnameSpan").style.display = "inline"
    }
}


function onMailChange(element) {
    regexp = /^[a-zA-Z\.@]*$/;
    if (regexp.test(element)) {
        document.getElementById("emailSpan").style.display = "none"
    } else {
        document.getElementById("emailSpan").style.display = "inline"
    }
}
function onCountryChange(element) {

    regexp = /^[а-яА-Я-\. ]{0,30}$|^[a-zA-Z-\. ]{0,30}$/;
    if (regexp.test(element)) {
        document.getElementById("countrySpan").style.display = "none"
    } else {
        document.getElementById("countrySpan").style.display = "inline"
    }
}
function onCityChange(element) {

    regexp = /^[а-яА-Я0-9-\. ]{0,30}$|^[a-zA-Z0-9-\. ]{0,30}$/;
    if (regexp.test(element)) {
        document.getElementById("citySpan").style.display = "none"
    } else {
        document.getElementById("citySpan").style.display = "inline"
    }
}

function onIndexChange(element) {

    regexp = /^[0-9]{0,6}$/;

    if (regexp.test(element)) {
        document.getElementById("indexSpan").style.display = "none"
    } else {
        document.getElementById("indexSpan").style.display = "inline"
    }
}
function onCountryCodeModalChange(element) {

    regexp = /^[0-9]{0,6}$/;
    if (regexp.test(element)) {
        document.getElementById("countryCodeModalSpan").style.display = "none"
    } else {
        document.getElementById("countryCodeModalSpan").style.display = "inline"
    }

}
function onOperatorCodeModalChange(element) {

    regexp = /^[0-9]{0,6}$/;
    if (regexp.test(element)) {
        document.getElementById("operatorCodeModalSpan").style.display = "none"
    } else {
        document.getElementById("operatorCodeModalSpan").style.display = "inline"
    }
}
function onTelNumberModalChange(element) {

    regexp = /^[0-9]{0,7}$/;

    if (regexp.test(element)) {
        document.getElementById("telNumberModalSpan").style.display = "none"
    } else {
        document.getElementById("telNumberModalSpan").style.display = "inline"
    }
}
function onTelTypeModalChange(element) {


    regexp = /^[a-zA-Z\sА-Яа-я]*$/;
    if (regexp.test(element)) {
        document.getElementById("telTypeModalSpan").style.display = "none"
    } else {
        document.getElementById("telTypeModalSpan").style.display = "inline"
    }
}


