/**
 * Created by Admin on 27.10.2017.
 */




function setAllChecked() {

    var checkedContacts = document.getElementById("userIds").checked;
    var selectedCheckboxes = document.getElementsByName("selected_contacts");

    // loop over them all and set checked
    for(var i = 0; i < selectedCheckboxes.length; i++){
        selectedCheckboxes[i].checked = checkedContacts;
    }
}

function deleteCheckedContacts() {

    var selectedCheckboxes = document.getElementsByName("selected_contacts");
    var checkBoxesCheckedContactIds = [];
    // loop over them all and push only selected values
    for (var i = 0; i < selectedCheckboxes.length; i++) {
        if (selectedCheckboxes[i].checked) {
            checkBoxesCheckedContactIds.push(selectedCheckboxes[i].value);
        }
    }

    if(checkBoxesCheckedContactIds.length === 0){
        return;
    }

    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/?Command=DeletePerson&contactIds=' + checkBoxesCheckedContactIds, false);
    xhr.send();
    window.location.reload();

}

function sendEmailToCheckedContacts(){
    var selectedCheckboxes = document.getElementsByName("selected_contacts");
    var checkBoxesCheckedContactIds = [];
    // loop over them all and push only selected values
    for (var i = 0; i < selectedCheckboxes.length; i++) {
        if (selectedCheckboxes[i].checked) {
            checkBoxesCheckedContactIds.push(selectedCheckboxes[i].value);
        }
    }

    if(checkBoxesCheckedContactIds.length === 0){
        return;
    }

    var ids = checkBoxesCheckedContactIds.toString();
    window.location.href = "/?Command=SendMailToCheckedContacts&contactIds=" + ids;
}

function showPerson(userId) {

    var formdata = new FormData;
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/?Command=ShowPersonDetails', false);
    formdata.append("userid",userId);
    xhr.send(formdata);

}

