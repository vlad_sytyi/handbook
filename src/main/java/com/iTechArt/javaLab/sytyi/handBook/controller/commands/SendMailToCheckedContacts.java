package com.iTechArt.javaLab.sytyi.handBook.controller.commands;

import com.iTechArt.javaLab.sytyi.handBook.controller.FrontCommand;
import com.iTechArt.javaLab.sytyi.handBook.entity.Person;
import com.iTechArt.javaLab.sytyi.handBook.service.EmailService;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;


public class SendMailToCheckedContacts extends FrontCommand {

    private EmailService emailService = new EmailService();

    @Override
    public void process() throws ServletException, IOException {


        String contactsIds = request.getParameter("contactIds");

        if (contactsIds != null){
            int [] idsForSendingEmail = parseContactIdsFromString(contactsIds);
            ArrayList<Person> personEmails = emailService.getEmailsByPersonId(idsForSendingEmail);
            request.setAttribute("personEmails",personEmails);
            forward(properties.getProperty("sendmail"));
        }
    }

    private int[] parseContactIdsFromString(String contactsIds) {

        String[] idsString = contactsIds.split(",");
        int[] ids = new int[idsString.length];

        for (int i = 0; i < ids.length; i++) {
            ids[i] = Integer.valueOf(idsString[i]);
        }
        return ids;
    }
}
