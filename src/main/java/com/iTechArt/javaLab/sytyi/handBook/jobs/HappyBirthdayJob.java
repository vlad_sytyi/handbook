package com.iTechArt.javaLab.sytyi.handBook.jobs;

import com.iTechArt.javaLab.sytyi.handBook.dao.impl.PersonDaoImpl;
import com.iTechArt.javaLab.sytyi.handBook.email.EmailManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import com.iTechArt.javaLab.sytyi.handBook.entity.Person;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

public class HappyBirthdayJob implements Job {

    PersonDaoImpl personDao = new PersonDaoImpl();
    EmailManager emailManager = new EmailManager();

    public HappyBirthdayJob() throws SchedulerException {


    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {


        //getting list birthdays and comparing with today date
        ArrayList<Person> personArrayList = personDao.getDataForBirthday();

        for (int i = 0; i < personArrayList.size(); i++) {
            String dateFromDb = String.valueOf(personArrayList.get(i).getBirthDate());
            if (dateFromDb.equals(getDateFormated())) {
                String message = personArrayList.get(i).getFirstName() + " " + personArrayList.get(i).getSecondName() + " has birthday";
                emailManager.sendMail(personArrayList.get(i).geteMail(), message, "Birthday");
            }
        }


    }
    private String getDateFormated() {
        java.util.Date date = new java.util.Date();
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }







}
