package com.iTechArt.javaLab.sytyi.handBook.dao;

import com.iTechArt.javaLab.sytyi.handBook.entity.Avatar;

import java.io.InputStream;


public interface AvatarDao {



    void createAvatar(InputStream inputStream,int id);
    
    Avatar readAvatar(int id);

    void updateAvatar(InputStream inputStream, int id);

    void deleteAvatar(int id);
}
