package com.iTechArt.javaLab.sytyi.handBook.controller.commands;

import com.iTechArt.javaLab.sytyi.handBook.controller.FrontCommand;
import com.iTechArt.javaLab.sytyi.handBook.service.impl.AttachmentsServiceImpl;
import java.io.IOException;



public class PersonAttachmentDownload extends FrontCommand {

    AttachmentsServiceImpl attachmentsService = new AttachmentsServiceImpl();

    @Override
    public void process() throws IOException {


            String attchmId = request.getParameter("attachmentid");

            byte[] attachment = attachmentsService.readAttachment(Integer.valueOf(attchmId));

            if (attachment != null){
                String s = attachmentsService.readFileName(Integer.valueOf(attchmId));
                String mimeType = "application/octet-stream";
                response.setContentType(mimeType);
                String headerKey = "Content-Disposition";
                String headerValue = String.format("attachment; filename=\"%s\"", s);
                response.setHeader(headerKey, headerValue);
                response.setContentLength(attachment.length);
                response.getOutputStream().write(attachment);
            }

    }
}
