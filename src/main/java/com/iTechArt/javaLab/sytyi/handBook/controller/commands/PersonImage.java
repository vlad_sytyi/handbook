package com.iTechArt.javaLab.sytyi.handBook.controller.commands;

import com.iTechArt.javaLab.sytyi.handBook.controller.FrontCommand;
import com.iTechArt.javaLab.sytyi.handBook.service.impl.AvatarServiceImpl;
import java.io.IOException;


public class PersonImage extends FrontCommand {


    AvatarServiceImpl avatarService;


    @Override
    public void process() throws IOException {


            String userId = request.getParameter("userid");
            avatarService = new AvatarServiceImpl();
            byte[] foto = avatarService.readAvatar(Integer.valueOf(userId));
            if (foto != null) {
                response.setContentType(servletContext.getMimeType("image"));
                response.setContentLength(foto.length);
                response.getOutputStream().write(foto);
            }

    }
}


