package com.iTechArt.javaLab.sytyi.handBook.dao.impl;

import com.iTechArt.javaLab.sytyi.handBook.dao.PersonDao;
import com.iTechArt.javaLab.sytyi.handBook.entity.*;
import com.iTechArt.javaLab.sytyi.handBook.utils.ConnectionPool;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;


public class PersonDaoImpl implements PersonDao {


    private Connection connection;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private static Logger logger = LoggerFactory.getLogger(PersonDaoImpl.class);

    private static void connectionClose(Connection connection) {

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.debug(e.getMessage() ,e);
            }
        }
    }

    private static void preparedStatementClose(PreparedStatement preparedStatement) {

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.debug(e.getMessage() ,e);
            }
        }
    }

    private static void resultSetClose(ResultSet resultSet) {

        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.debug(e.getMessage() ,e);
            }
        }
    }


    public void create(Person person) {

        try {
            connection = ConnectionPool.getInstance().getConnection();

            String query = "INSERT INTO  person (firstName,secondName,middleName,birthDate,sex," +
                    "maritalStatus,nationality,website,eMail,job,country,city,street,houseNumber,flatNumber,indexOfCity,isNew) " +
                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, person.getFirstName());
            preparedStatement.setString(2, person.getSecondName());
            preparedStatement.setString(3, person.getMiddleName());
            preparedStatement.setDate(4, person.getBirthDate());
            preparedStatement.setString(5, String.valueOf(person.getSex()));
            preparedStatement.setString(6, String.valueOf(person.getMaritalStatus()));
            preparedStatement.setString(7, person.getNationality());
            preparedStatement.setString(8, person.getWebsite());
            preparedStatement.setString(9, person.geteMail());
            preparedStatement.setString(10, person.getJob());
            preparedStatement.setString(11, person.getCountry());
            preparedStatement.setString(12, person.getCity());
            preparedStatement.setString(13, person.getStreet());
            preparedStatement.setInt(14, person.getHouseNumber());
            preparedStatement.setInt(15, person.getFlatNumber());
            preparedStatement.setInt(16, person.getIndex());
            preparedStatement.setString(17,person.getIsNew());
            preparedStatement.executeUpdate();


        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }
    }

    public Person read(int id) {

        Person person1 = new Person();

        try {

            connection = ConnectionPool.getInstance().getConnection();

            String query = "SELECT * FROM person WHERE id = ? AND isDeleted = b'0' ";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();


            while (resultSet.next()) {

                person1.setPersonId(resultSet.getInt(1));
                person1.setFirstName(resultSet.getString(2));
                person1.setSecondName(resultSet.getString(3));
                person1.setMiddleName(resultSet.getString(4));
                person1.setBirthDate(resultSet.getDate(5));
                person1.setSex(Sex.valueOf(resultSet.getString(6).toUpperCase()));
                person1.setMaritalStatus(MaritalStatus.valueOf(resultSet.getString(7).toUpperCase()));
                person1.setNationality(resultSet.getString(8));
                person1.setWebsite(resultSet.getString(9));
                person1.seteMail(resultSet.getString(10));
                person1.setJob(resultSet.getString(11));
                person1.setCountry(resultSet.getString(12));
                person1.setCity(resultSet.getString(13));
                person1.setStreet(resultSet.getString(14));
                person1.setHouseNumber(resultSet.getInt(15));
                person1.setFlatNumber(resultSet.getInt(16));
                person1.setIndex(resultSet.getInt(17));

            }

        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }

        return person1;
    }

    public void update(Person person, int id) {

        try {
            connection = ConnectionPool.getInstance().getConnection();

            String query = "UPDATE person SET firstName =?, secondName = ?, middleName = ?," +
                    "birthDate = ?, sex = ?, maritalStatus = ?, nationality = ?," +
                    "website = ?, eMail = ?, job = ?,country =?,city=?,street = ?,houseNumber =?," +
                    "flatNumber=?,indexOfCity=? WHERE id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setString(1, person.getFirstName());
            preparedStatement.setString(2, person.getSecondName());
            preparedStatement.setString(3, person.getMiddleName());
            preparedStatement.setDate(4, person.getBirthDate());
            preparedStatement.setString(5, String.valueOf(person.getSex()));
            preparedStatement.setString(6, String.valueOf(person.getMaritalStatus()));
            preparedStatement.setString(7, person.getNationality());
            preparedStatement.setString(8, person.getWebsite());
            preparedStatement.setString(9, person.geteMail());
            preparedStatement.setString(10, person.getJob());
            preparedStatement.setString(11, person.getCountry());
            preparedStatement.setString(12, person.getCity());
            preparedStatement.setString(13, person.getStreet());
            preparedStatement.setInt(14, person.getHouseNumber());
            preparedStatement.setInt(15, person.getFlatNumber());
            preparedStatement.setInt(16, person.getIndex());
            preparedStatement.setInt(17, id);

            preparedStatement.executeUpdate();


        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }

    }

    public void delete(int id) {

        try {
            connection = ConnectionPool.getInstance().getConnection();

            String query = "UPDATE person SET isDeleted = b'1' WHERE id =?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }

    }

    public Person readFullName(int id) {

        Person person1 = new Person();

        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "SELECT * FROM person";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                person1.setPersonId(resultSet.getInt("id"));
                person1.setFirstName(resultSet.getString("firstName"));
                person1.setSecondName(resultSet.getString("secondName"));
                person1.setMiddleName(resultSet.getString("middleName"));
                person1.setBirthDate(resultSet.getDate("birthDate"));
                person1.setJob(resultSet.getString("job"));
                System.out.println(person1.toString());
            }
        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }

        return person1;
    }



    public ArrayList<Person> searchPerson(String queryAdditions) {

        ArrayList<Person> list = new ArrayList<Person>();
        String query = "SELECT * FROM person WHERE isDeleted = b'0'";

        if (StringUtils.isNotEmpty(queryAdditions)) {
            query += queryAdditions;

        }


        try {
            connection = ConnectionPool.getInstance().getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                Person person = new Person();
                person.setPersonId(resultSet.getInt(1));
                person.setFirstName(resultSet.getString(2));
                person.setSecondName(resultSet.getString(3));
                person.setMiddleName(resultSet.getString(4));
                person.setBirthDate(resultSet.getDate(5));
                person.setSex(Sex.valueOf(resultSet.getString(6).toUpperCase()));
                person.setMaritalStatus(MaritalStatus.valueOf(resultSet.getString(7).toUpperCase()));
                person.setNationality(resultSet.getString(8));
                person.setWebsite(resultSet.getString(9));
                person.seteMail(resultSet.getString(10));
                person.setJob(resultSet.getString(11));
                person.setCountry(resultSet.getString(12));
                person.setCity(resultSet.getString(13));
                person.setStreet(resultSet.getString(14));
                person.setHouseNumber(resultSet.getInt(15));
                person.setFlatNumber(resultSet.getInt(16));
                person.setIndex(resultSet.getInt(17));
                list.add(person);

            }


        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }


        return list;
    }

    @Override
    public int numberOfPersons() {

        int numberOfPersons = 0;

        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "SELECT COUNT(id) AS person_count FROM person WHERE isDeleted = b'0'";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();


            if(resultSet.next()){
                numberOfPersons = resultSet.getInt(1);
            }


        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }


        return numberOfPersons;
    }

    @Override
    public ArrayList<Person> showPersons(int offset, int recordPerPage) {

        ArrayList<Person> personArrayList = new ArrayList<>();

        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "SELECT * FROM person WHERE  isDeleted = b'0' LIMIT ?, ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1,offset);
            preparedStatement.setInt(2,recordPerPage);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){

                Person person = new Person();
                person.setPersonId(resultSet.getInt(1));
                person.setFirstName(resultSet.getString(2));
                person.setSecondName(resultSet.getString(3));
                person.setMiddleName(resultSet.getString(4));
                person.setBirthDate(resultSet.getDate(5));
                person.setSex(Sex.valueOf(resultSet.getString(6).toUpperCase()));
                person.setMaritalStatus(MaritalStatus.valueOf(resultSet.getString(7).toUpperCase()));
                person.setNationality(resultSet.getString(8));
                person.setWebsite(resultSet.getString(9));
                person.seteMail(resultSet.getString(10));
                person.setJob(resultSet.getString(11));
                person.setCountry(resultSet.getString(12));
                person.setCity(resultSet.getString(13));
                person.setStreet(resultSet.getString(14));
                person.setHouseNumber(resultSet.getInt(15));
                person.setFlatNumber(resultSet.getInt(16));
                person.setIndex(resultSet.getInt(17));


                personArrayList.add(person);
            }


        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }

        return personArrayList;
    }

    @Override
    public ArrayList<Person> getPersonsEmailsByIds(String querryAttributes) {

        ArrayList<Person> personArrayList = new ArrayList<>();
        String query = "SELECT id,eMail FROM person WHERE isDeleted = b'0'";


        try {
            connection = ConnectionPool.getInstance().getConnection();
            String fullQuery = query+querryAttributes;
            PreparedStatement preparedStatement = connection.prepareStatement(fullQuery);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Person person = new Person();
                person.seteMail(resultSet.getString("eMail"));
                person.setPersonId(resultSet.getInt("id"));
                personArrayList.add(person);
            }

        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }

        return personArrayList;
    }

    @Override
    public ArrayList<Person> getDataForBirthday() {

        ArrayList<Person> personArrayList = new ArrayList<>();

        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "SELECT firstName,secondName,birthDate,eMail FROM person WHERE isDeleted = b'0'";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Person person = new Person();
                person.setFirstName(resultSet.getString("firstName"));
                person.setSecondName(resultSet.getString("secondName"));
                person.seteMail(resultSet.getString("eMail"));
                person.setBirthDate(resultSet.getDate("birthdate"));
                personArrayList.add(person);

            }


        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }

        return personArrayList;
    }


    public String getFullNameById(int id) {

        String firstName = "";
        String secondName = "";
        String fullName = "";

        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "SELECT firstName,secondName FROM person WHERE id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {

                 firstName = resultSet.getString("firstName");
                 secondName = resultSet.getString("secondName");
                 fullName = firstName + " " + secondName;

            }

        }catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }

        return fullName;
    }

    public Person readNewPerson(){

        Person person = new Person();

        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "SELECT * FROM person WHERE isNew = 'new' ";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()){
                person.setPersonId(resultSet.getInt("id"));
            }

        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }

        return person;
    }
    public void makeNewPersonNotNew(){



        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "UPDATE person SET isNew = '' WHERE isNew = 'new' ";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.execute();

        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }
    }




}
