package com.iTechArt.javaLab.sytyi.handBook.controller.commands;

import com.iTechArt.javaLab.sytyi.handBook.controller.FrontCommand;
import com.iTechArt.javaLab.sytyi.handBook.service.impl.PersonServiceImpl;


public class DeletePerson extends FrontCommand {

    private PersonServiceImpl personService = new PersonServiceImpl();
    @Override
    public void process() {


        String contactsIds = request.getParameter("contactIds");

        if (contactsIds != null){
            int [] idsForDeleting = parseContactIdsFromString(contactsIds);
            for (int i = 0; i<idsForDeleting.length; i++){
                personService.deletePerson(idsForDeleting[i]);
            }
        }



    }

    private int[] parseContactIdsFromString(String contactsIds) {

        String[] idsString = contactsIds.split(",");
        int[] ids = new int[idsString.length];

        for (int i = 0; i < ids.length; i++) {
            ids[i] = Integer.valueOf(idsString[i]);
        }
        return ids;
    }
}
