package com.iTechArt.javaLab.sytyi.handBook.service;

import java.io.InputStream;


public interface AvatarService {



    void saveAvatar(InputStream inputStream,int id);

    byte [] readAvatar(int id);

    void updateAvatar(InputStream inputStream, int id);

    void deleteAvatar(int id);


}
