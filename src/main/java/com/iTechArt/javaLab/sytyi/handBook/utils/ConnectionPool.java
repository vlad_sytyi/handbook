package com.iTechArt.javaLab.sytyi.handBook.utils;

import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;


public class ConnectionPool  {

    private static ConnectionPool connectionPool;
    private BasicDataSource dataSource;
    private static Logger logger = LoggerFactory.getLogger(ConnectionPool.class);
    private Properties properties;
    private static final String FILE_PATH = "database.properties";


    private ConnectionPool() throws IOException, SQLException, PropertyVetoException {
        properties = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(FILE_PATH);
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        }
        dataSource = new BasicDataSource();
        dataSource.setDriverClassName(properties.getProperty("db_driver"));
        dataSource.setUsername(properties.getProperty("db_login"));
        dataSource.setPassword(properties.getProperty("db_pass"));
        dataSource.setUrl(properties.getProperty("db_url"));
        dataSource.setMinIdle(30);
        dataSource.setMaxIdle(60);
        dataSource.setMaxOpenPreparedStatements(180);

    }

    public static ConnectionPool getInstance() throws IOException, SQLException, PropertyVetoException {
        if (connectionPool == null) {
            connectionPool = new ConnectionPool();
            return connectionPool;
        } else {
            return connectionPool;
        }
    }

    public Connection getConnection() throws SQLException {
        return this.dataSource.getConnection();
    }
}
