package com.iTechArt.javaLab.sytyi.handBook.dao.impl;

import com.iTechArt.javaLab.sytyi.handBook.dao.AttachmentsDao;
import com.iTechArt.javaLab.sytyi.handBook.entity.Attachments;
import com.iTechArt.javaLab.sytyi.handBook.utils.ConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Admin on 12.09.2017.
 */
public class AttachmentsDaoImpl implements AttachmentsDao {

    private Connection connection;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private static Logger logger = LoggerFactory.getLogger(AttachmentsDaoImpl.class);


    private static void connectionClose(Connection connection) {

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.debug(e.getMessage() ,e);
            }
        }
    }

    private static void preparedStatementClose(PreparedStatement preparedStatement) {

        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.debug(e.getMessage() ,e);
            }
        }
    }

    private static void resultSetClose(ResultSet resultSet) {

        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.debug(e.getMessage() ,e);
            }
        }
    }

    public void create(Attachments attachments) {


        try {
            connection = ConnectionPool.getInstance().getConnection();
            Blob blob = new javax.sql.rowset.serial.SerialBlob(attachments.getData());
            String query = "INSERT INTO attachments (fileName, dateOfLoad,coment,data,person_id) VALUES (?,?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, attachments.getAttachmentName());
            preparedStatement.setString(2, attachments.getDateOfLoad());
            preparedStatement.setString(3, attachments.getComment());
            preparedStatement.setBlob(4, blob);
            preparedStatement.setInt(5, attachments.getPersonId());
            preparedStatement.executeUpdate();


        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }


    }

    public Attachments read(int id) {

        Attachments attachments = new Attachments();

        try {

            connection = ConnectionPool.getInstance().getConnection();
            String query = "SELECT * FROM attachments WHERE id = ? AND isDeleted = b'0'";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                attachments.setId(resultSet.getInt(1));
                attachments.setAttachmentName(resultSet.getString(2));
                attachments.setDateOfLoad(resultSet.getString(3));
                attachments.setComment(resultSet.getString(4));
                Blob imageBlob = resultSet.getBlob("data");
                int blobLength = (int) imageBlob.length();
                byte[] blobAsBytes = imageBlob.getBytes(1, blobLength);
                imageBlob.free();
                attachments.setData(blobAsBytes);
            }

        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }
        return attachments;
    }

    public void update(Attachments attachments, int id) {

        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "UPDATE attachments SET  coment = ?,fileName = ? WHERE id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, attachments.getComment());
            preparedStatement.setString(2, attachments.getAttachmentName());
            preparedStatement.setInt(3, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }
    }

    public void delete(int id) {

        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "UPDATE attachments SET isDeleted = b'1' WHERE id =?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();


        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }

    }

    public String readFileName(int id) {

        String filename = null;
        try {

            connection = ConnectionPool.getInstance().getConnection();
            String query = "SELECT fileName FROM attachments WHERE id = ? ";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                filename = resultSet.getString("fileName");
            }


        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }
        return filename;
    }

    public ArrayList<Attachments> readAttachments() {

        ArrayList<Attachments> attachments = new ArrayList<>();

        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "SELECT * FROM attachments WHERE isDeleted = b'0' ";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                Attachments attachments1 = new Attachments();
                attachments1.setId(resultSet.getInt(1));
                attachments1.setAttachmentName(resultSet.getString(2));
                attachments1.setDateOfLoad(resultSet.getString(3));
                attachments1.setComment(resultSet.getString(4));
                Blob imageBlob = resultSet.getBlob("data");
                int blobLength = (int) imageBlob.length();
                byte[] blobAsBytes = imageBlob.getBytes(1, blobLength);
                imageBlob.free();
                attachments1.setData(blobAsBytes);

                attachments.add(attachments1);
            }

        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }


        return attachments;
    }

    public ArrayList<Attachments> readAttachments(int id) {

        ArrayList<Attachments> attachment = new ArrayList<>();

        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "SELECT * FROM attachments WHERE isDeleted = b'0' AND person_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                Attachments attachments = new Attachments();
                attachments.setId(resultSet.getInt(1));
                attachments.setAttachmentName(resultSet.getString(2));
                attachments.setDateOfLoad(resultSet.getString(3));
                attachments.setComment(resultSet.getString(4));
                Blob imageBlob = resultSet.getBlob("data");
                int blobLength = (int) imageBlob.length();
                byte[] blobAsBytes = imageBlob.getBytes(1, blobLength);
                imageBlob.free();
                attachments.setData(blobAsBytes);
                attachment.add(attachments);
            }

        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }


        return attachment;
    }

    public void deleteAllAttachments(int personId) {
        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "UPDATE attachments SET isDeleted = b'1' WHERE person_id =?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, personId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }
    }


    public ArrayList<Attachments> readAttachmentsNameIdCommentFields(int id) {

        ArrayList<Attachments> attachment = new ArrayList<>();

        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "SELECT id,fileName,coment FROM attachments WHERE isDeleted = b'0' AND person_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                Attachments attachments = new Attachments();
                attachments.setId(resultSet.getInt(1));
                attachments.setAttachmentName(resultSet.getString(2));
                attachments.setComment(resultSet.getString(4));
                attachment.add(attachments);
            }


        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }

        return attachment;
    }
}