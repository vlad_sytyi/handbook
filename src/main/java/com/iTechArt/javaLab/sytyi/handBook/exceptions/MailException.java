package com.iTechArt.javaLab.sytyi.handBook.exceptions;


public class MailException extends Exception {

    public MailException(){}

    public MailException(String message, Throwable exception){
        super(message, exception);
    }

    public MailException(String message){
        super(message);
    }

    public MailException(Throwable exception){
        super(exception);
    }
}
