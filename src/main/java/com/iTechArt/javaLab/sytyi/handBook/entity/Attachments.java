package com.iTechArt.javaLab.sytyi.handBook.entity;

import java.sql.Date;
import java.util.Arrays;


public class Attachments {

    private int id;
    private int personId;
    private String attachmentName;
    private String dateOfLoad;
    private String comment;
    private byte []  data;


    public Attachments() {
    }

    public Attachments(int id, int personId, String attachmentName, String dateOfLoad, String comment, byte[] data) {
        this.id = id;
        this.personId = personId;
        this.attachmentName = attachmentName;
        this.dateOfLoad = dateOfLoad;
        this.comment = comment;
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public String getDateOfLoad() {
        return dateOfLoad;
    }

    public void setDateOfLoad(String dateOfLoad) {
        this.dateOfLoad = dateOfLoad;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Attachments that = (Attachments) o;

        if (id != that.id) return false;
        if (personId != that.personId) return false;
        if (attachmentName != null ? !attachmentName.equals(that.attachmentName) : that.attachmentName != null)
            return false;
        if (dateOfLoad != null ? !dateOfLoad.equals(that.dateOfLoad) : that.dateOfLoad != null) return false;
        if (comment != null ? !comment.equals(that.comment) : that.comment != null) return false;
        return Arrays.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + personId;
        result = 31 * result + (attachmentName != null ? attachmentName.hashCode() : 0);
        result = 31 * result + (dateOfLoad != null ? dateOfLoad.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(data);
        return result;
    }

    @Override
    public String toString() {
        return "Attachments{" +
                "id=" + id +
                ", personId=" + personId +
                ", attachmentName='" + attachmentName + '\'' +
                ", dateOfLoad='" + dateOfLoad + '\'' +
                ", comment='" + comment + '\'' +
                ", data=" + Arrays.toString(data) +
                '}';
    }
}
