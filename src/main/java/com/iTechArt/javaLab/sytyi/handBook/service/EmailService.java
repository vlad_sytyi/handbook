package com.iTechArt.javaLab.sytyi.handBook.service;

import com.iTechArt.javaLab.sytyi.handBook.dao.impl.PersonDaoImpl;
import com.iTechArt.javaLab.sytyi.handBook.entity.Person;
import java.util.ArrayList;


public class EmailService  {

    private PersonDaoImpl personDao = new PersonDaoImpl();



    public ArrayList<Person> getEmailsByPersonId(int [] ids){

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" AND id IN (");
        for (int i =0; i<ids.length; i++){
            stringBuilder.append(ids[i]);
            if (i !=ids.length-1){
                stringBuilder.append(", ");
            }
        }
        stringBuilder.append(")");
        String queryAttr = new String(stringBuilder);

        return personDao.getPersonsEmailsByIds(queryAttr);
    }



}
