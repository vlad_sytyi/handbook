package com.iTechArt.javaLab.sytyi.handBook.controller.commands;

import com.iTechArt.javaLab.sytyi.handBook.controller.FrontCommand;
import javax.servlet.ServletException;
import java.io.IOException;


public class MailPage extends FrontCommand {


    @Override
    public void process() throws ServletException, IOException {
        forward(properties.getProperty("sendmail"));
    }
}
