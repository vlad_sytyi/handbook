package com.iTechArt.javaLab.sytyi.handBook.controller.commands;

import com.iTechArt.javaLab.sytyi.handBook.controller.FrontCommand;
import com.iTechArt.javaLab.sytyi.handBook.dao.impl.PersonDaoImpl;
import com.iTechArt.javaLab.sytyi.handBook.entity.Person;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;


public class ShowDataCommand extends FrontCommand {

   private PersonDaoImpl personDao = new PersonDaoImpl();
   private int page = 1;
   private int recordsPerPage = 10;
   private int numberOfPersons = personDao.numberOfPersons();
   private int numberOfPages = (int) Math.ceil(numberOfPersons * 1.0 / recordsPerPage);;

    public void process() throws ServletException, IOException {

        if (request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));

            ArrayList<Person> list = personDao.showPersons((page-1)*recordsPerPage,
                    recordsPerPage);
            request.setAttribute("noOfPages", numberOfPages);
            request.setAttribute("currentPage", page);
            request.setAttribute("usersList", list);
            forward(properties.getProperty("mainPage"));

        } else {

            ArrayList<Person> list = personDao.showPersons((page-1)*recordsPerPage,
                    recordsPerPage);

            request.setAttribute("page", page);
            request.setAttribute("currentPage", page);
            request.setAttribute("noOfPages", numberOfPages);
            request.setAttribute("usersList", list);

            forward(properties.getProperty("mainPage"));
        }





    }
}
