package com.iTechArt.javaLab.sytyi.handBook.service.impl;

import com.iTechArt.javaLab.sytyi.handBook.dao.impl.TelephoneDaoImpl;
import com.iTechArt.javaLab.sytyi.handBook.entity.Telephone;
import com.iTechArt.javaLab.sytyi.handBook.service.TelephoneService;

import java.util.ArrayList;


public class TelephoneServiceImpl implements TelephoneService {

    private TelephoneDaoImpl telephoneDao = new TelephoneDaoImpl();



    @Override
    public void synchroniseWithDataBase(ArrayList<Telephone> telephonesFromUI, int personId) {
        /*
        We know that Telephone's id  is generated in database.
        Thus we can say that all new phones are with Special id generated in UI by JS.
        THis id is 6699
        We create them and send it do dataBase.
         */
        ArrayList<Telephone> telephonesFromDataBaseWithoutNewTelephones = telephoneDao.readAllPhonesOfPerson(personId);
        for (int i = 0; i < telephonesFromUI.size(); i++) {

            if (telephonesFromUI.get(i).getTelephoneId() == 6699) {
                createTelephone(telephonesFromUI.get(i));
            }
        }



        /*
        We need 2 list the same size.
        First list  - is the list from UI. telephonesFromUI . (with telepfohnes with id 6699)
        Second list  - is the list from database without new telephones. telephonesFromDataBaseWithoutNewTelephones (all previous data. before modificating)

         */
        /*
        After adding new telephones, we need to delete all telephones, which was deleted in UI but we have them in dataBase

        ALL data will be deleted only by ID. that's why we need to extract IDs.
         */

        ArrayList<Integer> idsFromUI = new ArrayList<>();
        for (int i = 0; i < telephonesFromUI.size(); i++) {
            idsFromUI.add(telephonesFromUI.get(i).getTelephoneId());
        }
        ArrayList<Integer> idsFromDbOld = new ArrayList<>();
        for (int i = 0; i < telephonesFromDataBaseWithoutNewTelephones.size(); i++) {
            idsFromDbOld.add(telephonesFromDataBaseWithoutNewTelephones.get(i).getTelephoneId());
        }

        ArrayList<Integer> unionedId = new ArrayList<>(idsFromUI);
        unionedId.addAll(idsFromDbOld);
        ArrayList<Integer> intersectionListId = new ArrayList<>(idsFromUI);
        intersectionListId.retainAll(idsFromDbOld);
        unionedId.removeAll(intersectionListId);

        for (int i = 0; i < unionedId.size(); i++) {
            deleteTelephone(unionedId.get(i));
        }




        /*
           We have two loops.
           First loop is going through Telephones from UI.
           Second loop is going through Telephones from Data base new Telephones(id=6699).
           If we find Telephone from TelephonesFromUI with the same id as from TelephonesFromDataBase

           in this case we need to change id field in telephone

         */
        for (int i = 0; i < telephonesFromDataBaseWithoutNewTelephones.size(); i++) {
            for (int k = 0; k < telephonesFromUI.size(); k++) {
                if (telephonesFromDataBaseWithoutNewTelephones.get(i).getTelephoneId() == telephonesFromUI.get(k).getTelephoneId()) {
                    if (telephonesFromDataBaseWithoutNewTelephones.get(i).getCountryCode() != telephonesFromUI.get(k).getCountryCode()) {
                        updateTelephone(telephonesFromUI.get(k), telephonesFromUI.get(k).getTelephoneId());
                        break;
                    }
                    if (telephonesFromDataBaseWithoutNewTelephones.get(i).getProviderCode() != telephonesFromUI.get(k).getProviderCode()) {
                        updateTelephone(telephonesFromUI.get(k), telephonesFromUI.get(k).getTelephoneId());
                        break;
                    }
                    if (telephonesFromDataBaseWithoutNewTelephones.get(i).getTelephoneNumber() != telephonesFromUI.get(k).getTelephoneNumber()) {
                        updateTelephone(telephonesFromUI.get(k), telephonesFromUI.get(k).getTelephoneId());
                        break;
                    }
                    if (!(telephonesFromDataBaseWithoutNewTelephones.get(i).getComment()).equals(telephonesFromUI.get(k).getComment())) {
                        updateTelephone(telephonesFromUI.get(k), telephonesFromUI.get(k).getTelephoneId());
                        break;
                    }
                    if (!(telephonesFromDataBaseWithoutNewTelephones.get(i).getTelephoneType()).equals(telephonesFromUI.get(k).getTelephoneType())) {
                        updateTelephone(telephonesFromUI.get(k), telephonesFromUI.get(k).getTelephoneId());
                        break;
                    }
                }
            }


        }


    }

    @Override
    public void synchroniseWithDataBase(int personId) {

        deleteAllPersonsTelephones(personId);
    }


    @Override
    public void createTelephone(Telephone telephone) {

        telephoneDao.create(telephone);

    }

    @Override
    public void createTelephones(ArrayList<Telephone> telephonesFromUI, int personId) {

        /*
        We know that Telephone's id  is generated in database.
        Thus we can say that all new phones are with Special id generated in UI by JS.
        THis id is 6699
        We create them and send it do dataBase.
         */
        ArrayList<Telephone> telephonesFromDataBaseWithoutNewTelephones = telephoneDao.readAllPhonesOfPerson(personId);
        for (int i = 0; i < telephonesFromUI.size(); i++) {

            if (telephonesFromUI.get(i).getTelephoneId() == 6699) {
                createTelephone(telephonesFromUI.get(i));
            }
        }


    }


    @Override
    public ArrayList readAllTelephones(int personId) {
        return telephoneDao.readAllPhonesOfPerson(personId);
    }


    @Override
    public void updateTelephone(Telephone telephone, int id) {

        telephoneDao.update(telephone, id);
    }

    @Override
    public void deleteTelephone(int id) {

        telephoneDao.delete(id);
    }

    @Override
    public void deleteAllPersonsTelephones(int personId) {

        telephoneDao.deleteAllPhones(personId);
    }


}
