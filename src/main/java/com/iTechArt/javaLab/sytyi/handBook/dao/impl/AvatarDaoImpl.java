package com.iTechArt.javaLab.sytyi.handBook.dao.impl;

import com.iTechArt.javaLab.sytyi.handBook.dao.AvatarDao;
import com.iTechArt.javaLab.sytyi.handBook.entity.Avatar;
import com.iTechArt.javaLab.sytyi.handBook.utils.ConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;


public class AvatarDaoImpl implements AvatarDao{

    private static Logger logger = LoggerFactory.getLogger(AvatarDaoImpl.class);
    private Connection connection;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private static void connectionClose(Connection connection){

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.debug(e.getMessage() ,e);
            }
        }
    }

    private static void preparedStatementClose(PreparedStatement preparedStatement){

        if(preparedStatement != null){
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.debug(e.getMessage() ,e);
            }
        }
    }

    private static void resultSetClose(ResultSet resultSet){

        if(resultSet !=null){
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.debug(e.getMessage() ,e);
            }
        }
    }

    public void createAvatar(InputStream inputStream, int person_id) {


        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "INSERT INTO avatar (foto, person_id) values (?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setBlob(1,inputStream);
            preparedStatement.setInt(2,person_id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }

    }



    public Avatar readAvatar(int id) {

        Avatar avatar = new Avatar();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "SELECT foto FROM avatar WHERE person_id=? AND isDeleted = b'0'";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();

                if (resultSet.next()){
                    Blob imageBlob = resultSet.getBlob("foto");
                    int blobLength = (int) imageBlob.length();
                    byte[] blobAsBytes = imageBlob.getBytes(1, blobLength);
                    //release the blob and free up memory. (since JDBC 4.0)
                    imageBlob.free();
                    avatar.setByteArray(blobAsBytes);
                    avatar.setPersonId(id);
                }



        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }


        return avatar;
    }

    public void updateAvatar(InputStream inputStream, int id) {


        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "UPDATE avatar SET foto = ? WHERE person_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setBlob(1,inputStream);
            preparedStatement.setInt(2,id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }
    }

    @Override
    public void deleteAvatar(int id) {
        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "UPDATE avatar SET isDeleted = b'1' WHERE person_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }
    }


}
