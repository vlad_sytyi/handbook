package com.iTechArt.javaLab.sytyi.handBook.entity;


import java.util.Arrays;


public class Avatar  {

    private int avatarId;
    private int personId;
    private byte []  byteArray;

    public Avatar() {
    }

    public Avatar(int avatarId, int personId, byte[] byteArray) {
        this.avatarId = avatarId;
        this.personId = personId;
        this.byteArray = byteArray;
    }

    public int getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(int avatarId) {
        this.avatarId = avatarId;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public byte[] getByteArray() {
        return byteArray;
    }

    public void setByteArray(byte[] byteArray) {
        this.byteArray = byteArray;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Avatar avatar = (Avatar) o;

        if (avatarId != avatar.avatarId) return false;
        if (personId != avatar.personId) return false;
        return Arrays.equals(byteArray, avatar.byteArray);
    }

    @Override
    public int hashCode() {
        int result = avatarId;
        result = 31 * result + personId;
        result = 31 * result + Arrays.hashCode(byteArray);
        return result;
    }

    @Override
    public String toString() {
        return "Avatar{" +
                "avatarId=" + avatarId +
                ", personId=" + personId +
                ", byteArray=" + Arrays.toString(byteArray) +
                '}';
    }
}
