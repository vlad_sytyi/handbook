package com.iTechArt.javaLab.sytyi.handBook.utils;

import com.iTechArt.javaLab.sytyi.handBook.entity.Person;
import com.iTechArt.javaLab.sytyi.handBook.entity.Telephone;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.Part;
import java.sql.Date;
import java.util.ArrayList;


public class Validator {

    private static Logger logger = LoggerFactory.getLogger(Validator.class);


    private String JPEG = "image/jpeg";
    private String GIF = "image/gif";
    private String PNG = "image/png";
    private long MAX_QUERRY_SIZE = 4194304;

    public Validator() {

    }

    public ArrayList<String> validate(Person person) {

        ArrayList<String> errorMessages = new ArrayList<String>();

        if (person != null) {

            String firstname = person.getFirstName();
            String lastname = person.getSecondName();
            String middleName = person.getMiddleName();
            Date birthDate = person.getBirthDate();
            Enum sex = person.getSex();
            Enum maritalStatus = person.getMaritalStatus();
            String nationality = person.getNationality();
            String website = person.getWebsite();
            String eMail = person.geteMail();
            String job = person.getJob();
            String country = person.getCountry();
            String city = person.getCity();
            String street = person.getStreet();
            String houseNumber = String.valueOf(person.getHouseNumber());
            String flatNumber = String.valueOf(person.getFlatNumber());
            String index = String.valueOf(person.getIndex());

            if (StringUtils.isEmpty(firstname) || firstname.length() > 15) {
                logger.info("FirstName can't be empty, length must be less than 15");
                String errorFirtsname = "FirstName can't be empty, length must be less than 15. " +
                        "Please fill First name correctly";
                errorMessages.add(errorFirtsname);

            }
            if (StringUtils.isEmpty(lastname) || lastname.length() > 15) {
                logger.info("LastName can't be empty, length must be less than 15");
                String errorLastName = "LastName can't be empty and length must be less than 15. " +
                        "Please fill Last name correctly";
                errorMessages.add(errorLastName);
            }
            if (StringUtils.isEmpty(middleName) || middleName.length() > 15) {
                logger.info("middleName can't be empty, length must be less than 15");
                String errorMiddleName = "MiddleName can't be empty, length must be less than 15. " +
                        "Please fill Middle name field correctly ";
                errorMessages.add(errorMiddleName);
            }

            if (birthDate == null) {
                logger.info("Bitrhday can't be null");
                String errorBirthDay = "Bitrhday can't be null. Please fill BirthDate field correctly";
                errorMessages.add(errorBirthDay);
            }
            if (sex == null) {
                logger.info("Sex can't be null");
                String errorSex = "Sex can't be indenfined. Please fill Sex field correctly";
                errorMessages.add(errorSex);
            }
            if (maritalStatus == null) {
                logger.info("Marital status can't be null");
                String errorMaritalStatus = "Marital status can't be null. Please fill Marital status field correctly";
                errorMessages.add(errorMaritalStatus);
            }

            if (StringUtils.isEmpty(job) || job.length() > 30) {
                logger.info("job can't be empty, length must be less than 30");
                String errorJob = "Job can't be empty, length must be less than 15. Please fill job field correctly." +
                        " If you don't know job, write \"No job\" ";
                errorMessages.add(errorJob);
            }
            if (StringUtils.isEmpty(country) || country.length() > 30) {
                logger.info("country can't be empty, length must be less than 30");
                String errorCountry = "Country can't be empty, length must be less than 30. Please fill Country field correctly ";
                errorMessages.add(errorCountry);
            }
            if (StringUtils.isEmpty(city) || city.length() > 30) {
                logger.info("city can't be empty, length must be less than 30");
                String errorCity = "City can't be empty, length must be less than 30. Please fill City field correctly";
                errorMessages.add(errorCity);
            }
            if (StringUtils.isEmpty(houseNumber) || houseNumber.length() > 5) {
                logger.info("houseNumber can't be empty, length must be less than 5");
                String errorHouseNumber = "House Number can't be empty, length must be less than 5.  Please fill House Number field correctly";
                errorMessages.add(errorHouseNumber);
            }
            if (StringUtils.isEmpty(flatNumber) || flatNumber.length() > 5) {
                logger.info("flatNumber can't be empty, length must be less than 5");
                String errorFlatNumber = "Flat Number can't be empty, length must be less than 5. Please fill Flat Number field correctly";
                errorMessages.add(errorFlatNumber);
            }
            if (StringUtils.isEmpty(index) || index.length() != 6) {
                logger.info("index can't be empty, length must be 6 symbols");
                String errorIndex = "Index can't be empty, length must be 6 symbols. Please fill Index field correctly";
                errorMessages.add(errorIndex);
            }


        } else {
            logger.info("No attributes to Search");
            String errorAttribute = "There is no  attributes for searching out any contact. Please fill some field for searchinh criteria";
            errorMessages.add(errorAttribute);
        }


        return errorMessages;
    }

    public ArrayList<String> validateAttachment(Part part){

        ArrayList<String> errorMessages = new ArrayList<String>();
        long contentSize = part.getSize();

        if (contentSize > MAX_QUERRY_SIZE) {
            logger.info("Input file  is too large. Max size of file is 4194304 bytes");
            String errorAttribute = "Your file is too large for uploading. Max size of file is 4194304 bytes or 4MB ";
            errorMessages.add(errorAttribute);
        }
        return errorMessages;
    }

    public ArrayList<String> validate(Part part) {

        ArrayList<String> errorMessages = new ArrayList<String>();

        String contentType = part.getContentType();
        long contentSize = part.getSize();

        if (!contentType.equals(JPEG) && !contentType.equals(GIF) && !contentType.equals(PNG)) {
            logger.info("File has incorrect content type.It's not an image content");
            String errorAttribute = "Your file has incorrect type. Please use .JPG or .PNG or .GIF files";
            errorMessages.add(errorAttribute);
        }

        if (contentSize > MAX_QUERRY_SIZE) {
            logger.info("Input foto  is too large. Max size of file is 4194304 bytes");
            String errorAttribute = "Your photo is too large for uploading. Max size of file is 4194304 bytes or 4MB ";
            errorMessages.add(errorAttribute);
        }


        return errorMessages;
    }

    public ArrayList<String> validate(ArrayList<Telephone> telephones) {

        ArrayList<String> errorMessages = new ArrayList<String>();

        for (int i = 0; i < telephones.size(); i++) {

            if ((!StringUtils.isNumeric(String.valueOf(telephones.get(i).getProviderCode())) || (String.valueOf(telephones.get(i).getProviderCode())).length() > 6))
            {
                logger.info("Provider code is not number or lenght is more than 6 seymbols");
                String errorAttribute = "Provider code must be a  number  and lenght not more than 6 seymbols";
                errorMessages.add(errorAttribute);
            }
            if ((!StringUtils.isNumeric(String.valueOf(telephones.get(i).getCountryCode())) || (String.valueOf(telephones.get(i).getCountryCode())).length() > 6))
            {
                logger.info("Country code is not number or lenght is more than 6 seymbols");
                String errorAttribute = "Country code must be a number and lenght not more than 6 seymbols";
                errorMessages.add(errorAttribute);
            }
            if ((!StringUtils.isNumeric(String.valueOf(telephones.get(i).getTelephoneNumber())) || (String.valueOf(telephones.get(i).getTelephoneNumber())).length() > 7))
            {
                logger.info("Telephone number is not number or lenght is more than 7 seymbols");
                String errorAttribute = "Telephone must be a number and lenght not more than 7 seymbols";
                errorMessages.add(errorAttribute);
            }
            if ( (telephones.get(i).getTelephoneType()).length() > 30){
                logger.info("Telephone type has lenghts more than 30 symbols");
                String errorAttribute = "Telephone type can't have lenghts more than 30 symbols. ";
                errorMessages.add(errorAttribute);
            }
        }


        return errorMessages;
    }


}

