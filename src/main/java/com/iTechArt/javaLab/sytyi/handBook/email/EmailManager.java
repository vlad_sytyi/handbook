package com.iTechArt.javaLab.sytyi.handBook.email;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class EmailManager {


    private String username;
    private String pass;
    private Properties properties;
    private static Logger logger = LoggerFactory.getLogger(EmailManager.class);
    private static final String FILE_PATH = "mail.properties";

    public EmailManager() {
        initProperties();
        this.username = properties.getProperty("login");
        this.pass = properties.getProperty("pass");

    }

    public void sendMail(String  emails, String message, String subject){


        Thread thread = new Thread(new Sender(username,pass,subject,message,emails));
        thread.start();

    }

    protected void initProperties(){
        properties = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(FILE_PATH);
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        }
    }
}
