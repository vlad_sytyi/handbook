package com.iTechArt.javaLab.sytyi.handBook.controller.commands;

import com.google.common.io.ByteStreams;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.iTechArt.javaLab.sytyi.handBook.controller.FrontCommand;
import com.iTechArt.javaLab.sytyi.handBook.entity.*;
import com.iTechArt.javaLab.sytyi.handBook.service.TelephoneService;
import com.iTechArt.javaLab.sytyi.handBook.service.impl.AttachmentsServiceImpl;
import com.iTechArt.javaLab.sytyi.handBook.service.impl.AvatarServiceImpl;
import com.iTechArt.javaLab.sytyi.handBook.service.impl.PersonServiceImpl;
import com.iTechArt.javaLab.sytyi.handBook.service.impl.TelephoneServiceImpl;
import com.iTechArt.javaLab.sytyi.handBook.utils.Validator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.ServletException;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.*;




public class Upload extends FrontCommand {

    private static Logger logger = LoggerFactory.getLogger(Upload.class);

    private AvatarServiceImpl avatarService;

    @Override
    public void process() throws IOException, ServletException {


        Validator validator = new Validator();
        Date birthDate = null;
        Enum sex = null;
        Enum maritalStatus = null;
        int houseNumber = 0;
        int flatNumber = 0;
        int index = 0;
        String userId = request.getParameter("userId");
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String middleName = request.getParameter("middlename");
        String birthDateTest = request.getParameter("birthdate");

        if (StringUtils.isNotEmpty(birthDateTest)) {
            birthDate = Date.valueOf(request.getParameter("birthdate"));
        }
        String sextest = request.getParameter("sex");
        if (StringUtils.isNotEmpty(sextest)) {
            sex = Sex.valueOf(request.getParameter("sex").toUpperCase());
        }
        String maritalStatusTest = request.getParameter("marital status");
        if (StringUtils.isNotEmpty(maritalStatusTest)) {
            maritalStatus = MaritalStatus.valueOf(request.getParameter("marital status").toUpperCase());
        }

        String nationality = request.getParameter("nationality");
        String website = request.getParameter("website");
        String eMail = request.getParameter("email");
        String job = request.getParameter("job");
        String country = request.getParameter("country");
        String city = request.getParameter("city");
        String street = request.getParameter("street");


        String houseNumberTest = request.getParameter("houseNumber");

        if (StringUtils.isNumeric(houseNumberTest)) {
            houseNumber = Integer.valueOf(request.getParameter("houseNumber"));
        }

        String flatNumberTest = request.getParameter("flatNumber");
        if (StringUtils.isNumeric(flatNumberTest)) {
            flatNumber = Integer.valueOf(request.getParameter("flatNumber"));
        }

        String indexTest = request.getParameter("indexOfcity");
        if (StringUtils.isNumeric(indexTest)) {
            index = Integer.valueOf(request.getParameter("indexOfcity"));
        }


        ///////////////////////////////////////////////////////////////////////////
        /*
        Achieving Data for Avatar
         */
        ///////////////////////////////////////////////////////////////////////////
        Part filePart = request.getPart("avatar");

        if (filePart != null) {

            // if (filePart.getContentType().equals(TYPE_CONTENT_OCTET_STREAM)) {

                /*
                Cheching content of image div.
                if it is respone of PersonImage servlet - do nothing.
                if it is form-data image - check it, and save to databse
                 */


            ArrayList<String> errorsList = validator.validate(filePart);
            if (errorsList.size() == 0) {
                InputStream inputFile = null;

                inputFile = filePart.getInputStream();


                avatarService = new AvatarServiceImpl();
                if (avatarService.readAvatar(Integer.valueOf(userId)) == null) {
                    logger.info("Executing saveAvatar method");
                    avatarService.saveAvatar(inputFile, Integer.valueOf(userId));
                } else {
                    logger.info("Executing updateAvatar method");
                    avatarService.updateAvatar(inputFile, Integer.valueOf(userId));
                }
            } else {
                logger.info("Error with Avatar");
                request.setAttribute("errors", errorsList);
                forward(properties.getProperty("error"));
            }
        }


        ///////////////////////////////////////////////////////////////////
        /*
        TELEPHONES
         */
        ///////////////////////////////////////////////////////////////////
        /*
        Getting all telephones from Telephone table.
        If map of telephones is null - it means that we won't this person not to have any telephone number.
        creating variable numberOfTelephones.
        Then creating 2 arrays which will be the same size as array of Telephones.
        First array will be with Telephone type info
        Second array will be with Telephone.
        Creating List of Telephones and set all fields with data from arrays
         */

        TelephoneService telephoneService = new TelephoneServiceImpl();

        String telephoneUI = request.getParameter("telephones");


        if (telephoneUI != null) {
            Type itemsListType = new TypeToken<List<Telephone>>() {
            }.getType();
            List<Telephone> telephoneList = new Gson().fromJson(telephoneUI, itemsListType);

            for (int i = 0; i < telephoneList.size(); i++) {
                String numStr = telephoneList.get(i).getFullTelNumber();
                String[] s = numStr.split("-");
                telephoneList.get(i).setCountryCode(Integer.valueOf(s[0]));
                telephoneList.get(i).setProviderCode(Integer.valueOf(s[1]));
                telephoneList.get(i).setTelephoneNumber(Integer.valueOf(s[2]));
                telephoneList.get(i).setPersonId(Integer.valueOf(userId));
                telephoneList.get(i).setFullTelNumber(s[0] + s[1] + s[2]);
            }


            System.out.println(telephoneList.toString());
            ArrayList<String> errorsList = validator.validate((ArrayList<Telephone>) telephoneList);
            if (errorsList.size() == 0) {
                telephoneService.synchroniseWithDataBase((ArrayList<Telephone>) telephoneList, Integer.valueOf(userId));
            } else {
                request.setAttribute("errors", errorsList);
                forward(properties.getProperty("error"));
            }
        } else {
            //All person's telephones will be deleted
            telephoneService.synchroniseWithDataBase(Integer.valueOf(userId));

        }


        ///////////////////////////////////////////////////////
        /*
        ATTACHMENTS
         */
        ///////////////////////////////////////////////////////
        /*
        First of all we need to check for new files from Attachmnents table from UI.
        If we have any, we need to download it
        and create instance with filds attachmentName , data, date of load. put this instance ot list.
        then we need to parse JSON from UI and get comment if there any will be.
         if we have a comment comment to the instance from list.


         */
        Collection<Part> parts = request.getParts();
        String attachmentsUI = request.getParameter("attachments");
        AttachmentsServiceImpl attachmentsService = new AttachmentsServiceImpl();

        if (attachmentsUI != null) {
            Attachments attachmentsFromJson = new Attachments();
            Type itemsListType = new TypeToken<List<Attachments>>() {
            }.getType();
            List<Attachments> attachmentsList = new Gson().fromJson(attachmentsUI, itemsListType);
            ArrayList<Attachments> attachmentsArrayList = (ArrayList<Attachments>) attachmentsList;
            /*
            Checking for input files
            if we will have any file.
            check for Comment value and if there will be any add to Attachemnt
             */
            if (parts != null) {
                Attachments attachmentsNew = new Attachments();
                ArrayList<Attachments> arrayListWithNewAttachments = new ArrayList<>();
                for (Part p : parts) {
                    if (p.getName().equals("attachment-table-input")) {
                        InputStream is = p.getInputStream();
                        attachmentsNew.setData(ByteStreams.toByteArray(is));
                        attachmentsNew.setAttachmentName(extractFileName(p));
                        attachmentsNew.setDateOfLoad(getDateFormated());
                        attachmentsNew.setId(6699); //this is new attachment. mark it with id 6699
                        attachmentsNew.setPersonId(Integer.valueOf(userId));
                        arrayListWithNewAttachments.add(attachmentsNew);

                        for (int i = 0; i < attachmentsArrayList.size(); i++) {
                            for (int k = 0; k < arrayListWithNewAttachments.size(); k++) {
                                if (attachmentsArrayList.get(i).getId() == arrayListWithNewAttachments.get(k).getId()) {
                                    arrayListWithNewAttachments.get(k).setComment(attachmentsArrayList.get(i).getComment());
                                    attachmentsArrayList.remove(i); //remove our new attachemnt from arrayList attachmetns from ui
                                }
                            }

                        }
                        attachmentsService.addNewAttachment(arrayListWithNewAttachments);

                    }


                }

            }

             /*
            we don't have any new files
            we need to check for updating fields from UI
             */
            attachmentsService.synchroniseWithDataBase(attachmentsArrayList);


        } else {
            /*
            we don't have any data in UI
            We need to delete all attachments in DataBase
             */
            attachmentsService.deleteAllPersonAttachment(Integer.valueOf(userId));
        }


        ///////////////////////////////////////////////////////
        /*
        PERSON DATA
         */
        ///////////////////////////////////////////////////////

        Person person = new Person();
        person.setFirstName(firstname);
        person.setSecondName(lastname);
        person.setMiddleName(middleName);
        person.setBirthDate(birthDate);
        person.setSex(sex);
        person.setMaritalStatus(maritalStatus);
        person.setNationality(nationality);
        person.setWebsite(website);
        person.seteMail(eMail);
        person.setJob(job);
        person.setCountry(country);
        person.setCity(city);
        person.setStreet(street);
        person.setHouseNumber(houseNumber);
        person.setFlatNumber(flatNumber);
        person.setIndex(index);
        System.out.println(person.toString());

        ArrayList<String> errorsList = validator.validate(person);

        if (errorsList.size() == 0)

        {
            PersonServiceImpl personService = new PersonServiceImpl();
            personService.updatePerson(person, Integer.valueOf(userId));
            response.sendRedirect("/");

        } else

        {
            request.setAttribute("errors", errorsList);
            forward(properties.getProperty("error"));
        }


    }

    private String getDateFormated() {
        java.util.Date date = new java.util.Date();
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }


}
