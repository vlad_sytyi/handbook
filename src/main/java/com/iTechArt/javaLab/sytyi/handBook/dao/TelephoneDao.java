package com.iTechArt.javaLab.sytyi.handBook.dao;

import com.iTechArt.javaLab.sytyi.handBook.entity.Telephone;

import java.util.ArrayList;


public interface TelephoneDao {


    void create(Telephone telephone);

    Telephone read(int id);

    ArrayList<Telephone> readAllPhonesOfPerson(int id);

    void update(Telephone telephone, int id);

    void delete(int id);

    void deleteAllPhones(int personId);

}
