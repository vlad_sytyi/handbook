package com.iTechArt.javaLab.sytyi.handBook.dao;

import com.iTechArt.javaLab.sytyi.handBook.entity.Person;

import java.util.ArrayList;



public interface PersonDao extends Dao<Person> {


    Person readFullName(int id);
    String getFullNameById(int id);
    ArrayList<Person> searchPerson(String queryAdditions);
    int numberOfPersons();
    ArrayList<Person> showPersons(int offset, int recordPerPage);
    ArrayList<Person> getPersonsEmailsByIds(String querryAttributes);
    ArrayList<Person> getDataForBirthday();
}
