package com.iTechArt.javaLab.sytyi.handBook.service;

import com.iTechArt.javaLab.sytyi.handBook.entity.Person;
import com.iTechArt.javaLab.sytyi.handBook.entity.SearchCriteria;

import java.util.ArrayList;

public interface PersonService {


    void createPerson(Person person);

    Person readPerson(int id);

    void updatePerson(Person person,int id);

    void deletePerson(int id);

    ArrayList<Person> searchPerson(SearchCriteria searchCriteria);


}
