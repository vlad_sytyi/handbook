package com.iTechArt.javaLab.sytyi.handBook.entity;



public class Telephone {

    private int telephoneId;
    private int countryCode;
    private int providerCode;
    private int telephoneNumber;
    private String telephoneType;
    private String comment;
    private int personId;
    private String fullTelNumber;

    public Telephone() {
    }

    public Telephone(int telephoneId, int countryCode, int providerCode, int telephoneNumber, String telephoneType, String comment, int personId, String fullTelNumber) {
        this.telephoneId = telephoneId;
        this.countryCode = countryCode;
        this.providerCode = providerCode;
        this.telephoneNumber = telephoneNumber;
        this.telephoneType = telephoneType;
        this.comment = comment;
        this.personId = personId;
        this.fullTelNumber = fullTelNumber;
    }

    public int getTelephoneId() {
        return telephoneId;
    }

    public void setTelephoneId(int telephoneId) {
        this.telephoneId = telephoneId;
    }

    public int getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(int countryCode) {
        this.countryCode = countryCode;
    }

    public int getProviderCode() {
        return providerCode;
    }

    public void setProviderCode(int providerCode) {
        this.providerCode = providerCode;
    }

    public int getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(int telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getTelephoneType() {
        return telephoneType;
    }

    public void setTelephoneType(String telephoneType) {
        this.telephoneType = telephoneType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getFullTelNumber() {
        return fullTelNumber;
    }

    public void setFullTelNumber(String fullTelNumber) {
        this.fullTelNumber = fullTelNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Telephone telephone = (Telephone) o;

        if (telephoneId != telephone.telephoneId) return false;
        if (countryCode != telephone.countryCode) return false;
        if (providerCode != telephone.providerCode) return false;
        if (telephoneNumber != telephone.telephoneNumber) return false;
        if (personId != telephone.personId) return false;
        if (telephoneType != null ? !telephoneType.equals(telephone.telephoneType) : telephone.telephoneType != null)
            return false;
        if (comment != null ? !comment.equals(telephone.comment) : telephone.comment != null) return false;
        return fullTelNumber != null ? fullTelNumber.equals(telephone.fullTelNumber) : telephone.fullTelNumber == null;
    }

    @Override
    public int hashCode() {
        int result = telephoneId;
        result = 31 * result + countryCode;
        result = 31 * result + providerCode;
        result = 31 * result + telephoneNumber;
        result = 31 * result + (telephoneType != null ? telephoneType.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + personId;
        result = 31 * result + (fullTelNumber != null ? fullTelNumber.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Telephone{" +
                "telephoneId=" + telephoneId +
                ", countryCode=" + countryCode +
                ", providerCode=" + providerCode +
                ", telephoneNumber=" + telephoneNumber +
                ", telephoneType='" + telephoneType + '\'' +
                ", comment='" + comment + '\'' +
                ", personId=" + personId +
                ", fullTelNumber='" + fullTelNumber + '\'' +
                '}';
    }
}
