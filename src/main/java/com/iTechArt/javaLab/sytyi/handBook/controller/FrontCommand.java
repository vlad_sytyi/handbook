package com.iTechArt.javaLab.sytyi.handBook.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public abstract class FrontCommand {

    protected ServletContext servletContext;
    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected Properties properties;

    public static final String FILE_PATH = "path.properties";


    public abstract void process() throws IOException, ServletException;


    public void init(ServletContext servletContext, HttpServletRequest request, HttpServletResponse response) throws IOException {
        this.servletContext = servletContext;
        this.request = request;
        this.response = response;
        initProperties();
    }

    protected void forward(String pagePath) throws ServletException, IOException {
        RequestDispatcher dispatcher = servletContext.getRequestDispatcher(pagePath);

        dispatcher.forward(request, response);

    }


    protected void initProperties() throws IOException {
        properties = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(FILE_PATH);

        properties.load(inputStream);

    }

    protected String extractFileName(Part part) {
        String finalName = "";
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                finalName = s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return finalName;
    }



}
