package com.iTechArt.javaLab.sytyi.handBook.controller.commands;

import com.iTechArt.javaLab.sytyi.handBook.controller.FrontCommand;
import com.iTechArt.javaLab.sytyi.handBook.entity.*;
import com.iTechArt.javaLab.sytyi.handBook.service.impl.PersonServiceImpl;


import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;


public class SearchCommand extends FrontCommand {


    public void process() throws ServletException, IOException {


        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String middleName = request.getParameter("middlename");
        String birthDateFrom = request.getParameter("birthdatefrom");
        String birthDateTo = request.getParameter("birthdateto");
        String sex = request.getParameter("sex");
        String maritalStatus = request.getParameter("marital status");
        String country = request.getParameter("country");
        String city = request.getParameter("city");
        String street = request.getParameter("street");
        String houseNumber = request.getParameter("houseNumber");
        String flatNumber = request.getParameter("flatNumber");
        String index = request.getParameter("indexOfcity");


        SearchCriteria person = new SearchCriteria();
        person.setFirstName(firstname);
        person.setSecondName(lastname);
        person.setMiddleName(middleName);
        person.setBirthDateFrom(birthDateFrom);
        person.setBirthDateTo(birthDateTo);
        person.setSex(sex);
        person.setMaritalStatus(maritalStatus);
        person.setCountry(country);
        person.setCity(city);
        person.setStreet(street);
        person.setHouseNumber(houseNumber);
        person.setFlatNumber(flatNumber);
        person.setIndex(index);


        PersonServiceImpl personService = new PersonServiceImpl();
        ArrayList<Person> persons = personService.searchPerson(person);

        if (persons != null) {
            int page = 1;
            int recordsPerPage = 5;
            int numberOfPersons = persons.size();
            int numberOfPages = (int) Math.ceil(numberOfPersons * 1.0 / recordsPerPage);
            request.setAttribute("page", page);
            request.setAttribute("noOfPages", numberOfPages);
            request.setAttribute("currentPage", page);

        }

        request.setAttribute("usersList", persons);
        forward(properties.getProperty("mainPage"));
    }
}
