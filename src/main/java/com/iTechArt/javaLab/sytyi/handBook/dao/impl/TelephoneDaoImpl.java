package com.iTechArt.javaLab.sytyi.handBook.dao.impl;

import com.iTechArt.javaLab.sytyi.handBook.dao.TelephoneDao;
import com.iTechArt.javaLab.sytyi.handBook.entity.Telephone;
import com.iTechArt.javaLab.sytyi.handBook.utils.ConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Admin on 12.09.2017.
 */
public class TelephoneDaoImpl implements TelephoneDao {

    private Connection connection;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private static Logger logger = LoggerFactory.getLogger(TelephoneDaoImpl.class);
    private static void connectionClose(Connection connection){

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.debug(e.getMessage() ,e);
            }
        }
    }

    private static void preparedStatementClose(PreparedStatement preparedStatement){

        if(preparedStatement != null){
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                logger.debug(e.getMessage() ,e);
            }
        }
    }

    private static void resultSetClose(ResultSet resultSet){

        if(resultSet !=null){
            try {
                resultSet.close();
            } catch (SQLException e) {
                logger.debug(e.getMessage() ,e);
            }
        }
    }

    public void create(Telephone telephone) {

        try {
            connection = ConnectionPool.getInstance().getConnection();

            String query = "INSERT INTO telephone (countryCode, providerCode, telephoneNumber,telephoneType,coment,person_id) " +
                    "VALUES (?,?,?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, telephone.getCountryCode());
            preparedStatement.setInt(2, telephone.getProviderCode());
            preparedStatement.setInt(3, telephone.getTelephoneNumber());
            preparedStatement.setString(4, telephone.getTelephoneType());
            preparedStatement.setString(5, telephone.getComment());
            preparedStatement.setInt(6, telephone.getPersonId());
            preparedStatement.executeUpdate();


        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }
    }

    public Telephone read(int id) {

        Telephone telephone = new Telephone();


        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "SELECT * FROM telephone WHERE  isDeleted = b'0' AND id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){

                telephone.setTelephoneId(id);
                telephone.setPersonId(resultSet.getInt("person_id"));
                telephone.setCountryCode(resultSet.getInt(2));
                telephone.setProviderCode(resultSet.getInt(3));
                telephone.setTelephoneNumber(resultSet.getInt(4));

                telephone.setTelephoneType(resultSet.getString(5));
                telephone.setComment(resultSet.getString(6));
            }

        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }

        return telephone;
    }




    public void update(Telephone telephone, int id) {


        try {
            connection = ConnectionPool.getInstance().getConnection();

            String query = "UPDATE telephone SET countryCode = ?, providerCode = ?, telephoneNumber = ?" +
                    ",telephoneType = ?, coment = ? WHERE id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, telephone.getCountryCode());
            preparedStatement.setInt(2, telephone.getProviderCode());
            preparedStatement.setInt(3, telephone.getTelephoneNumber());
            preparedStatement.setString(4, telephone.getTelephoneType());
            preparedStatement.setString(5, telephone.getComment());
            preparedStatement.setInt(6, id );
            preparedStatement.executeUpdate();


        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }
    }

    public void delete(int id) {

        try {
            connection = ConnectionPool.getInstance().getConnection();

            String query = "UPDATE telephone SET isDeleted = b'1' WHERE  id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();


        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }

    }

    @Override
    public void deleteAllPhones(int personId) {

        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "UPDATE telephone SET isDeleted = b'1' WHERE person_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, personId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            connectionClose(connection);
        }

    }


    @Override
    public ArrayList<Telephone> readAllPhonesOfPerson(int id) {

        ArrayList<Telephone> telephonesList = new ArrayList<>();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            String query = "SELECT * FROM telephone WHERE  isDeleted = b'0' AND person_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Telephone telephone = new Telephone();
                telephone.setTelephoneId(resultSet.getInt(1));
                telephone.setCountryCode(resultSet.getInt(2));
                telephone.setProviderCode(resultSet.getInt(3));
                telephone.setTelephoneNumber(resultSet.getInt(4));
                telephone.setTelephoneType(resultSet.getString(5));
                telephone.setComment(resultSet.getString(6));
                telephone.setPersonId(resultSet.getInt(7));
                telephone.setFullTelNumber(String.valueOf((resultSet.getInt(2)))  + String.valueOf((resultSet.getInt(3))) + String.valueOf((resultSet.getInt(4))));
                telephonesList.add(telephone);

            }
        } catch (SQLException e) {
            logger.debug(e.getMessage() ,e);
        } catch (IOException e) {
            logger.debug(e.getMessage() ,e);
        } catch (PropertyVetoException e) {
            logger.debug(e.getMessage() ,e);
        } finally {
            preparedStatementClose(preparedStatement);
            resultSetClose(resultSet);
            connectionClose(connection);
        }

        return telephonesList;
    }


}
