package com.iTechArt.javaLab.sytyi.handBook.controller.commands;

import com.iTechArt.javaLab.sytyi.handBook.controller.FrontCommand;
import com.iTechArt.javaLab.sytyi.handBook.dao.impl.PersonDaoImpl;
import com.iTechArt.javaLab.sytyi.handBook.email.EmailManager;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroupDir;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;


public class SendMessage extends FrontCommand {

    private static Logger logger = LoggerFactory.getLogger(SendMessage.class);
    private final String BIRTHDAY_TEMPLATE = "Birthday";
    private final String CHRISTMAS_TEMPLATE = "Christmas";
    private STGroupDir stGroup = new STGroupDir("templates");
    private EmailManager emailManager = new EmailManager();
    private ArrayList<String> errorsList = new ArrayList<>();
    private PersonDaoImpl personDao = new PersonDaoImpl();

    @Override

    public void process() throws ServletException, IOException {

        Map<String, String[]> emailsMap = request.getParameterMap();
        Map<String, String[]> emailsIdsMap = request.getParameterMap();
        String message = request.getParameter("message");
        String subject = request.getParameter("subject");
        String template = request.getParameter("email-template");


        //check for emails
        if (emailsMap != null) {
            //checking subject of the letter
            if (StringUtils.isNotEmpty(subject)) {
                //checking template
                if (StringUtils.isNotEmpty(template)) {
                    String[] emails = emailsMap.get("emails");
                    String[] ids = emailsIdsMap.get("emailsId");
                    if (template.equals(BIRTHDAY_TEMPLATE)) {
                        for (int i = 0; i < emails.length; i++) {
                            //getting a fullname of person to insert it to our template
                            // creating a template and then send all data to email manager
                            ST st = stGroup.getInstanceOf("birthday");
                            st.add("name",personDao.getFullNameById(Integer.valueOf(ids[i])));
                            message = st.render();
                            emailManager.sendMail(emails[i],message,subject);
                            response.sendRedirect("/");
                        }

                    } else if (template.equals(CHRISTMAS_TEMPLATE)) {
                        //getting a fullname of person to insert it to our template
                        // creating a template and then send all data to email manager
                        for (int i = 0; i < emails.length; i++) {
                            ST st = stGroup.getInstanceOf("christmas");
                            st.add("name", personDao.getFullNameById(Integer.valueOf(ids[i])));
                            message = st.render();
                            emailManager.sendMail(emails[i], message, subject);
                            response.sendRedirect("/");
                        }
                    } else {
                        // something unpredictive wrong
                        String s = "There is no any template or message";
                        logger.error(s);
                        errorsList.add(s);
                        request.setAttribute("errors", errorsList);
                        forward(properties.getProperty("error"));
                    }


                } else {
                    //we don't have any template. thus we use message fron UI
                    if (StringUtils.isNotEmpty(message)) {
                        String[] emails = emailsMap.get("emails");
                        for (int i = 0; i < emails.length; i++) {
                            emailManager.sendMail(emails[i], message, subject);
                        }
                        response.sendRedirect("/");
                        //we don't have any template and we don't have message in UI. We need to create Error
                    } else {
                        String s = "There is no message in the letter";
                        logger.error(s);
                        errorsList.add(s);
                        request.setAttribute("errors", errorsList);
                        forward(properties.getProperty("error"));
                    }
                }

            } else {
                //We don't have any subjct. thus we need to create Error
                String s = "There is no subject for sending letter";
                logger.error(s);
                errorsList.add(s);
                request.setAttribute("errors", errorsList);
                forward(properties.getProperty("error"));
            }


        } else {
            //we don't have any emails to send a letter. thus we need to create Error
            String s = "There is no any email for sending letter";
            logger.error(s);
            errorsList.add(s);
            request.setAttribute("errors", errorsList);
            forward(properties.getProperty("error"));
        }
    }

}



