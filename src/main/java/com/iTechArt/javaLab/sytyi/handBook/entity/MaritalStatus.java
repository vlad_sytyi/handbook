package com.iTechArt.javaLab.sytyi.handBook.entity;

/**
 * Created by Admin on 10.09.2017.
 */
public enum MaritalStatus {

    SINGLE, MARRIED, WIDOWED
}
