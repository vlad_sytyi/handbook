package com.iTechArt.javaLab.sytyi.handBook.dao;




public interface Dao<T> {

    void create(T t);
    T read(int id );
    void update(T t,int id);
    void delete(int id );
}
