package com.iTechArt.javaLab.sytyi.handBook.controller.commands;

import com.iTechArt.javaLab.sytyi.handBook.controller.FrontCommand;
import com.iTechArt.javaLab.sytyi.handBook.entity.Person;
import com.iTechArt.javaLab.sytyi.handBook.entity.Telephone;
import com.iTechArt.javaLab.sytyi.handBook.service.impl.AttachmentsServiceImpl;
import com.iTechArt.javaLab.sytyi.handBook.service.impl.PersonServiceImpl;
import com.iTechArt.javaLab.sytyi.handBook.service.impl.TelephoneServiceImpl;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;


public class ShowPersonDetailsCommand extends FrontCommand {




   private Person person;
   private Telephone telephone;
   private PersonServiceImpl personService;
   private TelephoneServiceImpl telephoneService;
   private AttachmentsServiceImpl attachmentsService;


    public void process() throws ServletException, IOException {


        person = new Person();
        personService = new PersonServiceImpl();
        telephoneService = new TelephoneServiceImpl();
        attachmentsService = new AttachmentsServiceImpl();



        String userId = request.getParameter("userid");

        Person userData = personService.readPerson(Integer.valueOf(userId));
        ArrayList telephoneList = telephoneService.readAllTelephones(Integer.valueOf(userId));
        ArrayList attachmentList = attachmentsService.readAllAttachments(Integer.valueOf(userId));

        request.setAttribute("userData",userData);
        request.setAttribute("telephones",telephoneList);
        request.setAttribute("attachments",attachmentList);

        forward(properties.getProperty("personDetails"));
    }
}
