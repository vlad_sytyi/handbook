package com.iTechArt.javaLab.sytyi.handBook.service;

import com.iTechArt.javaLab.sytyi.handBook.entity.Telephone;

import java.util.ArrayList;

public interface TelephoneService {

    void createTelephone(Telephone telephone);

    void createTelephones(ArrayList<Telephone> telephones,int personId);

    ArrayList readAllTelephones(int personId);

    void updateTelephone(Telephone telephone, int id);

    void deleteTelephone(int id);

    void deleteAllPersonsTelephones (int personId);

    void synchroniseWithDataBase(ArrayList<Telephone> telephones,int personId);

    void synchroniseWithDataBase(int personId);

}
