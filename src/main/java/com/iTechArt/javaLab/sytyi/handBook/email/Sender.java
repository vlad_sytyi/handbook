package com.iTechArt.javaLab.sytyi.handBook.email;

import com.iTechArt.javaLab.sytyi.handBook.exceptions.MailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class Sender implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(Sender.class);
    private String username;
    private String password;
    private Properties props;
    private String subject;
    private String text;
    private String toEmail;

    public Sender(String username, String password,  String subject, String text, String toEmail) {
        this.username = username;
        this.password = password;
        this.subject = subject;
        this.text = text;
        this.toEmail = toEmail;

        props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
    }




    @Override
    public void run() {

        try {
            send();
        } catch (MailException e) {
            logger.debug(e.getMessage() ,e);
        }
    }

    public void send() throws MailException {


        Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }

        });

        try {
            Message message = new MimeMessage(session);
            //от кого
            message.setFrom(new InternetAddress(username));
            //кому
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
            //Заголовок письма
            message.setSubject(subject);
            //Содержимое
            message.setText(text);

            //Отправляем сообщение
            Transport.send(message);


        } catch (MessagingException e) {
            logger.debug(e.getMessage() , e);
            throw new RuntimeException(e);
        }


    }


}
