package com.iTechArt.javaLab.sytyi.handBook.service.impl;

import com.iTechArt.javaLab.sytyi.handBook.dao.impl.AttachmentsDaoImpl;
import com.iTechArt.javaLab.sytyi.handBook.dao.impl.PersonDaoImpl;
import com.iTechArt.javaLab.sytyi.handBook.dao.impl.TelephoneDaoImpl;
import com.iTechArt.javaLab.sytyi.handBook.entity.Person;
import com.iTechArt.javaLab.sytyi.handBook.entity.SearchCriteria;
import com.iTechArt.javaLab.sytyi.handBook.entity.Telephone;
import com.iTechArt.javaLab.sytyi.handBook.service.PersonService;
import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;



public class PersonServiceImpl implements PersonService {

    private PersonDaoImpl personDao = new PersonDaoImpl();
    private AttachmentsDaoImpl attachmentsDao = new AttachmentsDaoImpl();
    private TelephoneDaoImpl telephoneDao = new TelephoneDaoImpl();


    private ArrayList<Telephone> telephones;



    public void createPerson(Person person) {


        personDao.create(person);
    }


    public Person readPerson(int id) {
        Person person;
        person = personDao.read(id);
        return person;
    }

    public void updatePerson(Person person, int id) {


        personDao.update(person, id);

    }

    public void deletePerson(int personId) {
        personDao.delete(personId);
        attachmentsDao.deleteAllAttachments(personId);
        telephoneDao.deleteAllPhones(personId);
    }

    public ArrayList<Person> searchPerson(SearchCriteria searchCriteria) {

        StringBuilder queryBuilder = new StringBuilder();


        if (StringUtils.isNotEmpty(searchCriteria.getFirstName())) {
            queryBuilder.append(" AND firstname = \'");
            queryBuilder.append(searchCriteria.getFirstName());
            queryBuilder.append("\'");
        }
        if (StringUtils.isNotEmpty(searchCriteria.getSecondName())) {
            queryBuilder.append(" AND secondName = \'");
            queryBuilder.append(searchCriteria.getSecondName());
            queryBuilder.append("\'");
        }
        if (StringUtils.isNotEmpty(searchCriteria.getMiddleName())) {
            queryBuilder.append(" AND middleName = \'");
            queryBuilder.append(searchCriteria.getMiddleName());
            queryBuilder.append("\'");
        }

        if (StringUtils.isNotEmpty(searchCriteria.getBirthDateFrom()) || StringUtils.isNotEmpty(searchCriteria.getBirthDateTo())){

            queryBuilder.append(" AND (birthDate BETWEEN \'");
            queryBuilder.append(searchCriteria.getBirthDateFrom());
            queryBuilder.append("\'");
            queryBuilder.append(" AND \'");
            queryBuilder.append(searchCriteria.getBirthDateTo());
            queryBuilder.append("\')");

        }else if (StringUtils.isNotEmpty(searchCriteria.getBirthDateFrom()) || StringUtils.isEmpty(searchCriteria.getBirthDateTo())){
            queryBuilder.append(" AND birthdate > \'");
            queryBuilder.append(searchCriteria.getBirthDateFrom());
            queryBuilder.append("\'");
        }else  if (StringUtils.isEmpty(searchCriteria.getBirthDateFrom()) || StringUtils.isNotEmpty(searchCriteria.getBirthDateTo())){
            queryBuilder.append(" AND birthdate < \'");
            queryBuilder.append(searchCriteria.getBirthDateTo());
            queryBuilder.append("\'");
        }

        if (StringUtils.isNotEmpty(searchCriteria.getSex())) {
            queryBuilder.append(" AND sex = \'");
            queryBuilder.append(searchCriteria.getSex().toUpperCase());
            queryBuilder.append("\'");
        }

        if (StringUtils.isNotEmpty(searchCriteria.getMaritalStatus())) {
            queryBuilder.append(" AND maritalStatus = \'");
            queryBuilder.append(searchCriteria.getMaritalStatus().toUpperCase());
            queryBuilder.append("\'");
        }

        if (StringUtils.isNotEmpty(searchCriteria.getCountry())) {
            queryBuilder.append(" AND country = \'");
            queryBuilder.append(searchCriteria.getCountry());
            queryBuilder.append("\'");
        }

        if (StringUtils.isNotEmpty(searchCriteria.getCity())) {
            queryBuilder.append(" AND city = \'");
            queryBuilder.append(searchCriteria.getCity());
            queryBuilder.append("\'");
        }
        if (StringUtils.isNotEmpty(searchCriteria.getStreet())) {
            queryBuilder.append(" AND street =  \'");
            queryBuilder.append(searchCriteria.getStreet());
            queryBuilder.append("\'");
        }
        if (StringUtils.isNotEmpty(searchCriteria.getHouseNumber())) {
            queryBuilder.append(" AND houseNumber = \'");
            queryBuilder.append(searchCriteria.getHouseNumber());
            queryBuilder.append("\'");
        }
        if (StringUtils.isNotEmpty(searchCriteria.getFlatNumber())) {
            queryBuilder.append(" AND flatNumber = \'");
            queryBuilder.append(searchCriteria.getFlatNumber());
            queryBuilder.append("\'");
        }
        if (StringUtils.isNotEmpty(searchCriteria.getIndex())) {
            queryBuilder.append(" AND indexOfCity =  \'");
            queryBuilder.append(searchCriteria.getIndex());
            queryBuilder.append("\'");
        }


        ArrayList<Person> list = personDao.searchPerson(queryBuilder.toString());
        return list;
    }


}
