package com.iTechArt.javaLab.sytyi.handBook.controller;

import com.iTechArt.javaLab.sytyi.handBook.controller.commands.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10,      // 10MB
        maxRequestSize = 1024 * 1024 * 50)
public class FrontController extends HttpServlet {

    private static Logger logger = LoggerFactory.getLogger(FrontController.class);
    private static Map<String, Class> commands = new HashMap<>();

    static {
        commands.put("ShowPersonDetails", ShowPersonDetailsCommand.class);
        commands.put("ShowData", ShowDataCommand.class);
        commands.put("Search", SearchCommand.class);
        commands.put("PersonDetails", PersonDetailsCommand.class);
        commands.put("NewContact", NewContactCommand.class);
        commands.put("CreatePerson", CreatePersonCommand.class);
        commands.put("SearchPage", SearchPageCommand.class);
        commands.put("DeletePerson", DeletePerson.class);
        commands.put("Upload", Upload.class);
        commands.put("personImage", PersonImage.class);
        commands.put("personAttachment", PersonAttachmentDownload.class);
        commands.put("SendMailToCheckedContacts", SendMailToCheckedContacts.class);
        commands.put("MailPage", MailPage.class);
        commands.put("SendMessage", SendMessage.class);

    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        commandRequest(request, response);
    }

    private void commandRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String commandTest = request.getParameter("Command");
        if (StringUtils.isNotEmpty(commandTest)) {
            boolean access = commandCheck(commandTest);
            if (access) {
                for (String key : commands.keySet()) {
                    if (key.equals(commandTest)) {
                        FrontCommand command = getCommand(request.getParameter("Command"));
                        if (command != null) {
                            command.init(getServletContext(), request, response);
                            command.process();
                        }
                    }
                }
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Wrong Command");
            }
        } else {
            FrontCommand command = getCommand("ShowData");
            if (command != null) {
                command.init(getServletContext(), request, response);
                command.process();
            }
        }


    }


    public static FrontCommand getCommand(String command) {

        FrontCommand frontCommand = null;
        Class commandsClass = (Class) commands.get(command);

        if (commandsClass != null) {
            try {
                frontCommand = (FrontCommand) commandsClass.newInstance();
            } catch (Exception e) {
                logger.debug(e.getMessage(), e);
            }
        }

        return frontCommand;
    }

    private boolean commandCheck(String command) {

        boolean accessTrue = false;
        List<String> commandsInMap = new ArrayList<String>(commands.keySet());
        for (int i = 0; i < commandsInMap.size(); i++) {
            if (commandsInMap.get(i).equals(command)) {
                accessTrue = true;
            }
        }


        return accessTrue;
    }


}
