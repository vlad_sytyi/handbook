package com.iTechArt.javaLab.sytyi.handBook.service.impl;

import com.iTechArt.javaLab.sytyi.handBook.dao.impl.AvatarDaoImpl;
import com.iTechArt.javaLab.sytyi.handBook.entity.Avatar;
import com.iTechArt.javaLab.sytyi.handBook.service.AvatarService;
import java.io.InputStream;



public class AvatarServiceImpl implements AvatarService {

    private AvatarDaoImpl avatarDao = new AvatarDaoImpl();




    public void saveAvatar(InputStream inputStream, int personId) {

        if (inputStream != null || personId != 0) {
            avatarDao.createAvatar(inputStream, personId);


        }

    }

    @Override
    public byte[] readAvatar(int id) {

        Avatar avatar;
        byte[] bytes = null;
        avatar = avatarDao.readAvatar(id);
        if (avatar.getByteArray() != null){
            bytes = avatar.getByteArray();
        }
        return bytes;
    }

    @Override
    public void updateAvatar(InputStream inputStream, int id) {

        if (inputStream != null || id != 0) {
            avatarDao.updateAvatar(inputStream,id);
        }
    }

    @Override
    public void deleteAvatar(int id) {

    }
}
