package com.iTechArt.javaLab.sytyi.handBook.entity;

import java.sql.Date;



public class Person {

    private int personId;
    private String firstName;
    private String secondName;
    private String middleName;
    private Date birthDate;
    private Enum sex;
    private Enum maritalStatus;
    private String nationality;
    private String website;
    private String eMail;
    private String job;
    private String country;
    private String city;
    private String street;
    private int houseNumber;
    private int flatNumber;
    private int index;
    private String isNew;

    public Person() {
    }

    public Person(int personId, String firstName, String secondName, String middleName, Date birthDate, Enum sex, Enum maritalStatus, String nationality, String website, String eMail, String job, String country, String city, String street, int houseNumber, int flatNumber, int index, String isNew) {
        this.personId = personId;
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = middleName;
        this.birthDate = birthDate;
        this.sex = sex;
        this.maritalStatus = maritalStatus;
        this.nationality = nationality;
        this.website = website;
        this.eMail = eMail;
        this.job = job;
        this.country = country;
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
        this.flatNumber = flatNumber;
        this.index = index;
        this.isNew = isNew;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Enum getSex() {
        return sex;
    }

    public void setSex(Enum sex) {
        this.sex = sex;
    }

    public Enum getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(Enum maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    public int getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(int flatNumber) {
        this.flatNumber = flatNumber;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getIsNew() {
        return isNew;
    }

    public void setIsNew(String isNew) {
        this.isNew = isNew;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (personId != person.personId) return false;
        if (houseNumber != person.houseNumber) return false;
        if (flatNumber != person.flatNumber) return false;
        if (index != person.index) return false;
        if (firstName != null ? !firstName.equals(person.firstName) : person.firstName != null) return false;
        if (secondName != null ? !secondName.equals(person.secondName) : person.secondName != null) return false;
        if (middleName != null ? !middleName.equals(person.middleName) : person.middleName != null) return false;
        if (birthDate != null ? !birthDate.equals(person.birthDate) : person.birthDate != null) return false;
        if (sex != null ? !sex.equals(person.sex) : person.sex != null) return false;
        if (maritalStatus != null ? !maritalStatus.equals(person.maritalStatus) : person.maritalStatus != null)
            return false;
        if (nationality != null ? !nationality.equals(person.nationality) : person.nationality != null) return false;
        if (website != null ? !website.equals(person.website) : person.website != null) return false;
        if (eMail != null ? !eMail.equals(person.eMail) : person.eMail != null) return false;
        if (job != null ? !job.equals(person.job) : person.job != null) return false;
        if (country != null ? !country.equals(person.country) : person.country != null) return false;
        if (city != null ? !city.equals(person.city) : person.city != null) return false;
        if (street != null ? !street.equals(person.street) : person.street != null) return false;
        return isNew != null ? isNew.equals(person.isNew) : person.isNew == null;
    }

    @Override
    public int hashCode() {
        int result = personId;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + (maritalStatus != null ? maritalStatus.hashCode() : 0);
        result = 31 * result + (nationality != null ? nationality.hashCode() : 0);
        result = 31 * result + (website != null ? website.hashCode() : 0);
        result = 31 * result + (eMail != null ? eMail.hashCode() : 0);
        result = 31 * result + (job != null ? job.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (street != null ? street.hashCode() : 0);
        result = 31 * result + houseNumber;
        result = 31 * result + flatNumber;
        result = 31 * result + index;
        result = 31 * result + (isNew != null ? isNew.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "personId=" + personId +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthDate=" + birthDate +
                ", sex=" + sex +
                ", maritalStatus=" + maritalStatus +
                ", nationality='" + nationality + '\'' +
                ", website='" + website + '\'' +
                ", eMail='" + eMail + '\'' +
                ", job='" + job + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", houseNumber=" + houseNumber +
                ", flatNumber=" + flatNumber +
                ", index=" + index +
                ", isNew='" + isNew + '\'' +
                '}';
    }
}
