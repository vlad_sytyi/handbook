package com.iTechArt.javaLab.sytyi.handBook.service;

import com.iTechArt.javaLab.sytyi.handBook.entity.Attachments;


public interface AttachmentsService{


    void saveAttachment(Attachments attachments);

    byte [] readAttachment(int id);

    void updateAttachment(Attachments attachments ,int id);

    void deleteAttachment(int id);


}
