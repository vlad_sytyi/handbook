package com.iTechArt.javaLab.sytyi.handBook.service.impl;

import com.iTechArt.javaLab.sytyi.handBook.dao.impl.AttachmentsDaoImpl;
import com.iTechArt.javaLab.sytyi.handBook.entity.Attachments;
import com.iTechArt.javaLab.sytyi.handBook.service.AttachmentsService;

import java.util.ArrayList;


public class AttachmentsServiceImpl implements AttachmentsService {

   private AttachmentsDaoImpl attachmentsDao = new AttachmentsDaoImpl();
   private ArrayList<Attachments> attachmentsFromDB = attachmentsDao.readAttachments();

    public void addNewAttachment(ArrayList<Attachments> attachments){

        /*
        We know that Attachments's id  is generated in database.
        Thus we can say that all new attachments are with Special id generated in UI by JS.
        THis id is 6699
        We create them and send it do dataBase.
         */

        for (int i = 0; i < attachments.size(); i++) {

            if (attachments.get(i).getId() == 6699) {
                saveAttachment(attachments.get(i));
            }
        }

    }

    public void synchroniseWithDataBase(ArrayList<Attachments> attachments) {



        /*
        We need 2 list the same size.
        First list  - is the list from UI. attachments .
        Second list  - is the list from database without new attachments.
        we need to delete all attachments, which was deleted in UI but we have them in dataBase
        ALL data will be deleted only by ID. that's why we need to extract IDs.

         */

        ArrayList<Integer> idsFromUI = new ArrayList<>();
        for (int i = 0; i < attachments.size(); i++) {
            idsFromUI.add(attachments.get(i).getId());
        }
        ArrayList<Integer> idsFromDbOld = new ArrayList<>();
        for (int i = 0; i < attachmentsFromDB.size(); i++) {
            idsFromDbOld.add(attachmentsFromDB.get(i).getId());
        }
        ArrayList<Integer> unionedId = new ArrayList<>(idsFromUI);
        unionedId.addAll(idsFromDbOld);

        ArrayList<Integer> intersectionListId = new ArrayList<>(idsFromUI);
        intersectionListId.retainAll(idsFromDbOld);

        unionedId.removeAll(intersectionListId);

        for (int i = 0; i < unionedId.size(); i++) {
            deleteAttachment(unionedId.get(i));
        }


        /*

              We have two loops.
           First loop is going through attachments from UI.
           Second loop is going through attachments from Data base.
           If we find attachments  with the same id , it will be checked for updates


         */


        for (int i = 0; i < attachmentsFromDB.size(); i++) {
            for (int k = 0; k < attachments.size(); k++) {
                if (attachmentsFromDB.get(i).getId() == attachments.get(k).getId()) {
                    if (!(attachmentsFromDB.get(i).getComment()).equals(attachments.get(k).getComment())) {
                        updateAttachment(attachments.get(k), attachments.get(k).getId());
                        break;
                    }
                    if (!(attachmentsFromDB.get(i).getAttachmentName()).equals(attachments.get(k).getAttachmentName())) {
                        updateAttachment(attachments.get(k), attachments.get(k).getId());
                        break;
                    }
                }
            }
        }


    }


    @Override
    public void saveAttachment(Attachments attachments) {

        attachmentsDao.create(attachments);

    }

    @Override
    public byte[] readAttachment(int id) {

        Attachments attachments = attachmentsDao.read(id);

        return attachments.getData();
    }

    @Override
    public void updateAttachment(Attachments attachment, int id) {

        attachmentsDao.update(attachment, id);
    }

    @Override
    public void deleteAttachment(int id) {

        attachmentsDao.delete(id);
    }

    public String readFileName(int id) {

        return attachmentsDao.readFileName(id);
    }

    public ArrayList<Attachments> readAllAttachments(int id) {

        return attachmentsDao.readAttachments(id);
    }

    public void deleteAllPersonAttachment(int id) {

        attachmentsDao.deleteAllAttachments(id);
    }
}
