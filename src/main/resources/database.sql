DROP DATABASE IF EXISTS handbook;
CREATE DATABASE  handbook
  CHARACTER SET 'utf8'
  COLLATE 'utf8_general_ci';
USE handbook;

CREATE TABLE person (

  id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  firstName VARCHAR(45) NOT NULL ,
  secondName VARCHAR(45) NOT NULL ,
  middleName VARCHAR(45) NOT NULL ,
  birthDate DATE NOT NULL ,
  sex VARCHAR(45) NOT NULL ,
  maritalStatus VARCHAR(45)  ,
  nationality VARCHAR(45)  ,
  website VARCHAR(45)  ,
  eMail VARCHAR(45) ,
  job VARCHAR(45),
  country VARCHAR(45),
  city VARCHAR(45),
  street VARCHAR(45),
  houseNumber INT(11),
  flatNumber INT(11),
  indexOfCity INT(11),
  isDeleted bit(1) NOT NULL DEFAULT b'0'


) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;


 CREATE TABLE avatar(

   id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
   foto LONGBLOB,
   person_id int(11),
   isDeleted bit(1) NOT NULL DEFAULT b'0',
   FOREIGN KEY (person_id) REFERENCES person(id)
   ON UPDATE CASCADE
   ON DELETE CASCADE

 )ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE telephone (

  id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  countryCode INT(11),
  providerCode INT(11),
  telephoneNumber INT(11),
  telephoneType VARCHAR(45),
  coment VARCHAR(45),
  person_id int(11),
  isDeleted bit(1) NOT NULL DEFAULT b'0',
  FOREIGN KEY (person_id) REFERENCES person(id)
    ON UPDATE CASCADE
    ON DELETE CASCADE

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE attachments (

  id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  fileName VARCHAR(45),
  dateOfLoad VARCHAR(45) ,
  coment VARCHAR(45),
  data  LONGBLOB,
  person_id int(11),
  isDeleted bit(1) NOT NULL DEFAULT b'0',
  FOREIGN KEY (person_id) REFERENCES person(id)
    ON UPDATE CASCADE
    ON DELETE CASCADE


)ENGINE=InnoDB DEFAULT CHARSET=utf8;





